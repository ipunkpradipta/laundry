<?php
defined('BASEPATH') or exit('No direct script access allowed');
ini_set('smtp_port', 25);
class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->library('email');
    }

    public function index()
    {
        if (isset($this->session->userdata('user_detail')['user_email'])) {
            redirect('home/dashboard');
        }

        $data = [
            'title' => 'Login',
        ];
        $this->load->view('layout/auth_header', $data);
        $this->load->view('auth/login');
        $this->load->view('layout/auth_footer');
    }

    public function register()
    {
        $data = [
            'title' => 'Register',
        ];
        $this->load->view('layouts/auth_header', $data);
        $this->load->view('auth/register');
        $this->load->view('layouts/auth_footer');
    }

    public function login()
    {
        $this->form_validation->set_rules('signin-email', 'Email', 'trim|required');
        $this->form_validation->set_rules('signin-password', 'Password', 'trim|required|min_length[5]');

        if ($this->form_validation->run() == FALSE) {
            $return = ['status' => false, 'message' => validation_errors()];
        } else {
            $email = $this->input->post('signin-email');
            $password = $this->input->post('signin-password');
            $return = $this->_login($email, $password);
            if ($return['s'] == 'success') {
                $return = ['status' => true, 'message' => 'Login Berhasil'];
            } else {
                $return = ['status' => false, 'message' => 'Login Gagal, Harap cek email atau password anda!'];
            }
        }

        echo json_encode($return);
    }

    private function _login($email, $password)
    {
        $user = $this->db->get_where('m_user', ['email' => $email])->row_array();
        //Jika usernya ada
        if ($user) {
            //cek password nya
            if (password_verify($password, $user['password'])) {
                $data = [
                    'user_detail' => [
                        'user_id' => $user['id'],
                        'user_nama' => $user['nama'],
                        'user_email' => $user['email'],
                        'user_image' => $user['image'],
                        'user_is_admin' => $user['status_admin'],
                    ],
                    'user_menu' => $this->getRoleMenu($user['id'])
                ];
                $this->session->set_userdata($data);
                //update last login
                $this->db->update('m_user', ['login_terakhir' => date('Y-m-d H:i:s'), 'login_browser' => getBrowserName()['name'] . ' V' . getBrowserName()['version'] . ' on ' . getBrowserName()['platform']]);
                $return = ['s' => 'success', 'm' => 'Login Success!'];
            } else {
                $return = ['s' => 'fail', 'm' => 'Wrong password!'];
            }
        } else {
            $return = ['s' => 'fail', 'm' => 'Email is not registered!'];
        }

        return $return;
    }

    public function doRegist()
    {
        $this->form_validation->set_rules('signup-name', 'Nama', 'trim|required');
        $this->form_validation->set_rules('signup-email', 'Email', 'trim|required');
        $this->form_validation->set_rules('signup-password', 'Password', 'trim|required|min_length[5]');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('register_status', validation_errors());
            redirect(base_url('auth/register'));
        } else {
            $email = $this->input->post('signup-email', true);
            $data = [
                'nama' => htmlspecialchars($this->input->post('signup-name', true)),
                'email' => htmlspecialchars($email),
                'password' => password_hash($this->input->post('signup-password', true), PASSWORD_DEFAULT),
            ];
            //Siapkan Token
            $token = base64_encode(random_bytes(32));
            $user_token = [
                'email' => $email,
                'token' => $token,
            ];

            // $this->db->insert('m_user_token', $user_token);

            // $this->_sendEmail($token, 'verify');
            if ($this->crud->createData('m_user', $data)) {
                $this->session->set_flashdata('login_status', 'Akun sudah didaftarkan, silahkan cek email untuk aktivasi!');
                redirect(base_url('auth'));
            };
        }
    }

    public function regist()
    {
        $email = $this->input->post('email', true);
        $password = $this->input->post('password');
        $r_password = $this->input->post('rpassword');
        $cek_email = $this->crud->readDataObject('db', 'email', 'user', ['email' => $email], [], '', '', '');
        if (count($cek_email) == 1) {
            $return = ['s' => 'fail', 'm' => 'Email has been registered!'];
        } else {
            if ($password != $r_password) {
                $return = ['s' => 'fail', 'm' => 'Password must be same with Confirm Password!'];
            } else {
                $data = [
                    'name' => htmlspecialchars($this->input->post('fullname', true)),
                    'email' => htmlspecialchars($email),
                    'image' => 'default.png',
                    'password' => password_hash($password, PASSWORD_DEFAULT),
                    'role_id' => 2,
                    'is_active' => 0,
                    'date_created' => time()
                ];
                if ($this->crud->createData('w_master', 'user', $data)) {
                    $return = ['s' => 'success', 'm' => 'Data successfully registered!'];
                };
            }
        }
        echo json_encode($return);
    }

    private function _sendEmail($token, $type)
    {
        // $config = [
        //     'protocol'  => 'smtp',
        //     'smtp_host' => 'ssl://mail.sartrans.net',
        //     'smtp_user' => 'no_reply@sartrans.net',
        //     'smtp_pass' => 'Pradipta123',
        //     'smtp_port' => 465,
        //     'mailtype'  => 'html',
        //     'charset' => 'utf-8',
        //     'newline' => "\r\n"
        // ];
        $config = [
            'protocol'  => 'mail',
            'smtp_host' => 'ssl://smtp.gmail.com',
            'smtp_user' => 'inomosinfo@gmail.com',
            'smtp_pass' => 'Pradipta123',
            'smtp_port' => 465,
            'mailtype'  => 'html',
            'charset' => 'utf-8',
            'newline' => "\r\n"
        ];

        $this->email->initialize($config);

        $this->email->from('inomosinfo@gmail.com', 'INOMOS');
        $this->email->to($this->input->post('email'));

        if ($type == 'verify') {
            $this->email->subject('Account Verification');
            $this->email->message('Click this link to verify your account : <a href="' . base_url() . 'auth/verify?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Activate</a>');
        } else if ($type == 'forgot') {
            $this->email->subject('Reset Password');
            $this->email->message('Click this link to reset your Password : <a href="' . base_url() . 'auth/resetpassword?email=' . $this->input->post('email') . '&token=' . urlencode($token) . '">Reset Password</a>');
        }

        if ($this->email->send()) {
            return true;
        } else {
            echo $this->email->print_debugger();
            die;
        }
    }

    public function verify()
    {
        $email = $this->input->get('email');
        $token = rawurldecode(urlencode($this->input->get('token')));

        $user = $this->db->get_where('user', ['email' => $email])->row_array();

        if ($user) {
            $user_token = $this->db->get_where('user_token', ['token' => $token])->row_array();
            if ($user_token) {
                if (time() - strtotime($user_token['date_created']) < (60 * 60 * 24)) {
                    $this->w_master->set('is_active', 1);
                    $this->w_master->where('email', $email);
                    $this->w_master->update('user');
                    $this->w_master->delete('user_token', ['email' => $email]);
                    $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">' . $email . ' has been activared! Please login.</div>');
                    redirect('auth');
                } else {
                    $this->w_master->delete('user', ['email' => $email]);
                    $this->w_master->delete('user_token', ['email' => $email]);
                    $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Account activation failed! Token expired.</div>');
                    redirect('auth');
                }
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Account activation failed! Wrong token.</div>');
                redirect('auth');
            }
        } else {
            $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">Account activation failed! Wrong email.</div>');
            redirect('auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('user_detail');
        $this->session->set_flashdata('login_status', 'Logout Berhasil!');
        redirect('auth');
    }

    public function blocked()
    {
        $this->load->view('auth/blocked');
    }

    function cekEmail()
    {
        $cek_user = $this->crud->readDataObject('db', 'id', 'user', ['email' => $this->input->post('email'), 'is_active' => 1], [], '', '', '');
        if (count($cek_user) == 1) {
            $return = ['s' => 'success', 'm' => 'Found'];
        } else {
            $return = ['s' => 'fail', 'm' => 'Not Found!'];
        }
        echo json_encode($return);
    }

    function getRoleMenu($idUser)
    {
        $joinTable[0]['table'] = 'm_user_menu b';
        $joinTable[0]['relation'] = 'a.id_menu = b.id';
        $listMenuAsigned = $this->crud->readDataObject('b.*', 'm_user_menu_map a', ['a.id_user' => $idUser], $joinTable, 'LEFT', '', 'show_sequent', 'ASC');
        $menuAsigned = [];
        if (count($listMenuAsigned) > 0) {
            foreach ($listMenuAsigned as $menu) {
                $submenuAsigned = [];
                $joinLagi[0]['table'] = 'm_user_submenu b';
                $joinLagi[0]['relation'] = 'a.id_submenu = b.id';
                $listSubmenuAsigned = $this->crud->readDataObject('b.*', 'm_user_submenu_map a', ['a.id_user' => $idUser, 'a.id_menu' => $menu->id], $joinLagi, 'LEFT', '', '', '');
                if (count($listSubmenuAsigned) > 0) {
                    foreach ($listSubmenuAsigned as $submenu) {
                        $submenuAsigned[] = [
                            'submenuId' => $submenu->id,
                            'submenuIdMenu' => $submenu->id_menu,
                            'submenuName' => $submenu->name,
                            'submenuIcon' => $submenu->icon,
                            'submenuUrl' => $submenu->url
                        ];
                    }
                }
                $menuAsigned[] = [
                    'menuId' => $menu->id,
                    'menuName' => $menu->name,
                    'menuIcon' => $menu->icon,
                    'menuUrl' => $menu->url,
                    'isParent' => $menu->is_parent,
                    'submenu' => $submenuAsigned,
                ];
            }
        }
        return $menuAsigned;
    }
}

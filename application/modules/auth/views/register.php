<div id="wrapper" class="d-flex align-items-center justify-content-center">
    <div class="auth-box register">
        <div class="content">
        <div class="header">
            <div class="logo text-center"><img src="<?= base_url('assets')?>/klorofilpro/assets/images/logo-dark.png" alt="Klorofil Logo"></div>
            <p class="lead">Create an account</p>
        </div>
        <form class="form-auth-small" method="post" action="<?=base_url('auth/doRegist')?>">
            <?= $this->session->flashdata('register_status');?>
            <div class="form-group">
                <label for="signup-name" class="control-label sr-only">Nama</label>
                <input type="name" name="signup-name" class="form-control" id="signup-name" placeholder="Masukan Nama">
            </div>
            <div class="form-group">
                <label for="signup-email" class="control-label sr-only">Email</label>
                <input type="email" name="signup-email" class="form-control" id="signup-email" placeholder="Masukan Email">
            </div>
            <div class="form-group">
                <label for="signup-password" class="control-label sr-only">Password</label>
                <input type="password" name="signup-password" class="form-control" id="signup-password" placeholder="Masukan Password">
            </div>
            <button type="submit" class="btn btn-primary btn-lg btn-block">REGISTER</button>
            <div class="bottom">
                <span class="helper-text">Already have an account? <a href="<?= base_url('auth/login')?>">Login</a></span>
            </div>
        </form>
<!-- 
        <div class="separator-linethrough"><span>OR</span></div>

        <button class="btn btn-signin-social"><i class="fa fa-facebook-official facebook-color"></i> Sign in with Facebook</button>
        <button class="btn btn-signin-social"><i class="fa fa-twitter twitter-color"></i> Sign in with Twitter</button>
        </div> -->
    </div>
</div>
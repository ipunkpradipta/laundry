<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="row mb-2">
            <div class="col-sm-4">
                <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#modalAddMenu" onclick="setForm('menu')" class="btn btn-danger mb-2"><i class="mdi mdi-plus-circle me-2"></i> Tambah Konsumen</a>
            </div>
        </div>
        <table id="table_customer" class="table dt-responsive nowrap w-100">
          <thead>
            <tr>
              <th>Nama Konsumen</th>
              <th>No HP</th>
              <th>Alamat</th>
              <th>Created</th>
            </tr>
          </thead>
        </table>
      </div> <!-- end card body-->
    </div> <!-- end card -->
  </div><!-- end col-->
</div>
<!-- end row-->

<div class="modal fade" id="modalAddMenu" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myCenterModalLabel">Form Input</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <form action="#" method="post" id="form_customer_add">
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="nama" name="nama" placeholder="Alexandre Christie" autocomplete="off">
                    <label for="nama">Nama Konsumen</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="no_handphone" name="no_handphone" placeholder="0857xxx" autocomplete="off">
                    <label for="no_handphone">No Handphone</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="alamat" name="alamat" placeholder="Tangerang Raya" autocomplete="off">
                    <label for="alamat">Alamat</label>
                </div>
                <button type="submit" id="menu-submit-btn" class="btn btn-warning waves-effect waves-light" data-toggle="reload">Submit</button>
                <button class="btn btn-warning" id="btn-spinner" type="button" disabled="" style="display:none">
                    <span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>
                    Loading...
                </button>
              </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- <script src="<?= base_url('assets/ubold')?>/assets/js/vendor.min.js"></script>
<script src="<?= base_url('assets/ubold')?>/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script> -->
<!-- <script>
  const tableCustomer = $("#table_customer").DataTable({
      language: {
          paginate: {
              previous: "<i class='mdi mdi-chevron-left'>",
              next: "<i class='mdi mdi-chevron-right'>",
          },
      },
      drawCallback: function () {
          $(".dataTables_paginate > .pagination").addClass("pagination-rounded");
      },
      serverSide: true,
      proccessing: true,
      ajax:{
          url:'<?=base_url()?>master/customer/getAllCustomer',
          method:'POST'
      }
  });
  
  $('#form_customer_add').on('submit',(e) => {
        e.preventDefault();
        const formData = $('#form_customer_add').serialize();
        $.ajax({
            url:'<?=base_url()?>master/customer/postCustomer',
            method:'POST',
            data: formData,
            dataType:'json',
            beforeSend: () => {
                $('#menu-submit-btn').hide();
                $('#btn-spinner').show();
            },
            success:(result) => {
                console.log(result)
                $('#menu-submit-btn').show();
                $('#btn-spinner').hide();
                $("#modalAddMenu .btn-close").click()
                if(result.status){
                    tableCustomer.ajax.reload();
                    notif_success('Sukses!',result.message);
                    $('#form_customer_add').get(0).reset();
                }else{
                    tableCustomer.ajax.reload();
                    $('#form_customer_add').get(0).reset();
                    notif_danger('Gagal!',result.message)
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                $('#menu-submit-btn').show();
                $('#btn-spinner').hide();
                $("#modalAddMenu .btn-close").click()
                alert(xhr.status);
                alert(thrownError);
            }
        })
    })
</script> -->
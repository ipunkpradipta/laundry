<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="row mb-2">
            <div class="col-sm-4">
                <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#modalAddMenu" onclick="setForm('menu')" class="btn btn-danger mb-2"><i class="mdi mdi-plus-circle me-2"></i> Tambah Menu</a>
            </div>
        </div>
        <table id="table_menu" class="table dt-responsive nowrap w-100">
          <thead>
            <tr>
              <th>Nama</th>
              <th>Icon</th>
              <th>Url</th>
              <th>Submenu</th>
              <th>Created</th>
            </tr>
          </thead>
        </table>
      </div> <!-- end card body-->
    </div> <!-- end card -->
  </div><!-- end col-->
</div>
<!-- end row-->

<div class="modal fade" id="modalAddMenu" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myCenterModalLabel">Form Input</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <form action="#" method="post" id="form_menu_add">
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="nama_menu" name="nama_menu" placeholder="Menu" autocomplete="off">
                    <input type="hidden" class="form-control" id="form_input" name="form_input">
                    <input type="hidden" class="form-control" id="id_menu" name="id_menu">
                    <label for="nama_menu">Nama Menu</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="icon" name="icon" placeholder="SVG Icon" autocomplete="off">
                    <label for="icon">Icon</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="url" name="url" placeholder="#" autocomplete="off">
                    <label for="url">Alamat Uri</label>
                </div>
                <div class="form-check mb-3">
                    <input type="checkbox" class="form-check-input" id="is_parent" name="is_parent">
                    <label class="form-check-label" for="is_parent">Punya Submenu ?</label>
                </div>
                <button type="submit" id="menu-submit-btn" class="btn btn-warning waves-effect waves-light" data-toggle="reload">Submit</button>
                <button class="btn btn-warning" id="btn-spinner" type="button" disabled="" style="display:none">
                    <span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>
                    Loading...
                </button>
              </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalListSubmenu" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myCenterModalLabel">Daftar Submenu</h4>
                <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#modalAddMenu" onclick="setForm('submenu')" class="btn btn-danger" style="margin-left:10px"><i class="mdi mdi-plus-circle me-2"></i> Tambah Submenu</a>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <table id="table_submenu" class="table dt-responsive nowrap w-100">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Icon</th>
                    <th>Url</th>
                    <th>Submenu</th>
                    <th>Created</th>
                  </tr>
                </thead>
              </table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="row mb-2">
            <div class="col-sm-4">
                <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#modalAddProduct" class="btn btn-danger mb-2"><i class="mdi mdi-plus-circle me-2"></i> Tambah Produk</a>
            </div>
        </div>
        <table id="table_product" class="table dt-responsive nowrap w-100">
          <thead>
            <tr>
              <th>Nama Produk</th>
              <th>Satuan</th>
              <th>Harga</th>
              <th>Status</th>
              <th>Created</th>
              <th>#</th>
            </tr>
          </thead>
        </table>
      </div> <!-- end card body-->
    </div> <!-- end card -->
  </div><!-- end col-->
</div>
<!-- end row-->

<div class="modal fade" id="modalAddProduct" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myCenterModalLabel">Form Input</h4>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <form action="#" method="post" id="form_product_add">
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="nama_produk" name="nama_produk" placeholder="Menu" autocomplete="off">
                    <label for="nama_produk">Nama Produk</label>
                </div>
                <div class="form-floating mb-3">
                    <select name="satuan" id="satuan" class="form-control">
                      <option value="KG">KG</option>
                      <option value="PCS">PCS</option>
                    </select>
                    <label for="satuan">Satuan</label>
                </div>
                <div class="form-floating mb-3">
                    <select name="duration" id="duration" class="form-control">
                      <?php 
                      foreach ($durations as $duration) {
                        echo '<option value="'.$duration->duration_in_day.'">'.$duration->nama.'</option>';
                      }
                      ?>
                    </select>
                    <label for="duration">Lama Pengerjaan</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="harga" name="harga" placeholder="#" autocomplete="off">
                    <label for="harga">Harga</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="diskon_item" name="diskon_item" placeholder="#" autocomplete="off">
                    <label for="diskon_item">Diskon (%)</label>
                </div>
                <button type="submit" id="menu-submit-btn" class="btn btn-warning waves-effect waves-light" data-toggle="reload">Submit</button>
                <button class="btn btn-warning" id="btn-spinner" type="button" disabled="" style="display:none">
                    <span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>
                    Loading...
                </button>
              </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalListSubmenu" tabindex="-1" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myCenterModalLabel">Daftar Submenu</h4>
                <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#modalAddMenu" onclick="setForm('submenu')" class="btn btn-danger" style="margin-left:10px"><i class="mdi mdi-plus-circle me-2"></i> Tambah Submenu</a>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
              <table id="table_submenu" class="table dt-responsive nowrap w-100">
                <thead>
                  <tr>
                    <th>Nama</th>
                    <th>Icon</th>
                    <th>Url</th>
                    <th>Submenu</th>
                    <th>Created</th>
                  </tr>
                </thead>
              </table>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script>
  var rupiah = document.getElementById('harga');
  rupiah.addEventListener('keyup', function(e){
    rupiah.value = formatRupiah(this.value, 'Rp. ');
  });

  function formatRupiah(angka, prefix){
    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split   		= number_string.split(','),
    sisa     		= split[0].length % 3,
    rupiah     		= split[0].substr(0, sisa),
    ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
      separator = sisa ? '.' : '';
      rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
  }
</script>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		is_logged_in();
		$this->load->library(['Home_Library','form_validation','upload']);
		$this->load->helper('dev');
	}

    public function index()
    {
        $data['title'] = 'User Management';
        $data['breadcrumb'] = ['left' => 'Pengguna','right' => ['App','Master','Perngguna']];
        $this->home_library->main('user/index',$data);
    }

    function getAllUser()
	{
		$column_order = array('id');
		$column_search = array('id');
		$order = array('id');
		$list = $this->crud->getDatatable('*','m_user',[],$column_search,$column_order,$order,[]);
		$data = array();
		foreach ($list as $row) {
			$color = $row->status_active == '1' ? 'success':'info';
			$label = $row->status_active == '1' ? 'Aktif':'Tidak aktif';
			$button =  $row->status_active == '1' ? '<span class="badge label-table bg-'.$color.'">'.$label.'</span>' : '<span class="badge label-table bg-'.$color.'">'.$label.'</span>';
			$sub_array = array();
			$sub_array[] = $row->nama;
			$sub_array[] = $row->email;
			$sub_array[] = $row->image;
			$sub_array[] = $button;
			$sub_array[] = $row->login_terakhir;
			$sub_array[] = $row->created_date;
			$sub_array[] = '<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
								<div class="btn-group btn-group-sm" style="float: none;">
									<button type="button" onclick="openRoleModal('.$row->id.')" class="tabledit-edit-button btn btn-success" style="float: none;">
										<span class="mdi mdi-pencil"></span>
									</button>
								</div>
							</div>';
			$data[] = $sub_array;
		}

		$output = array(
			'draw'	        => $_POST['draw'],
			'recordsTotal'	=> $this->crud->dataTableCount('m_user',[],[]),
			'recordsFiltered'	=> $this->crud->dataTableFilter('*','m_user',[],$column_search,$column_order,$order,[]),
			'data'		=> $data
		);
		echo json_encode($output);
	}

	function getRoleMenu()
	{
		$idUser = $this->input->post('idUser');
		
		$listMenuAsigned = $this->crud->readDataObject('*','m_user_menu_map',['id_user'=> $idUser],[],'','','','');
		$idMenuAsigned = [];
		if(count($listMenuAsigned) > 0){
			foreach($listMenuAsigned as $menuAsigned){
				$idMenuAsigned[] = $menuAsigned->id_menu;
			}
		}
		
		$return = [];
		$listMenu = $this->crud->readDataObject('*','m_user_menu',[],[],'','','','');
		if(count($listMenu) > 0){
			foreach($listMenu as $menu){
				$idSubmenuAsigned = [];
				$listSubmenuAsigned = $this->crud->readDataObject('*','m_user_submenu_map',['id_user' => $idUser,'id_menu'=>$menu->id],[],'','','','');
				if(count($listSubmenuAsigned) > 0){
					foreach($listSubmenuAsigned as $submenuAsigned){
						$idSubmenuAsigned[] = $submenuAsigned->id_submenu;
					}
				}
				$listSubmenuReturn = [];
				$listSubmenu = $this->crud->readDataObject('*','m_user_submenu',['id_menu' => $menu->id],[],'','','','');
				foreach($listSubmenu as $submenu){
					$listSubmenuReturn[] = [
						'id' => $submenu->id,
						'menuId' => $submenu->id_menu,
						'name' => $submenu->name,
						'assigned' => in_array($submenu->id, $idSubmenuAsigned),
					];
				}
				
				$return[] = [
					'menuId'=> $menu->id,
					'menuName'=> $menu->name,
					'menuAsign'=> in_array($menu->id,$idMenuAsigned),
					'submenu'=> $listSubmenuReturn,
				];
			}
		}
		echo json_encode(['status' => true,'message' => 'Sukses Mengambil Data','data' => $return]);
	}

	function updateRoleMenu()
	{
		$currentUserLogin = $this->session->userdata('user_detail')['user_nama'];

		$this->form_validation->set_rules('mapping', 'Mapping', 'trim|required');
		$this->form_validation->set_rules('userId', 'Mapping', 'trim|required');
		$this->form_validation->set_rules('menuId', 'Mapping', 'trim|required');
		
		
		if ($this->form_validation->run() == false) {
			$return = ['status' => false,'message' => validation_errors()];
		} else {
			$mapping = $this->input->post('mapping');
			if($mapping == 'menu'){
				$table = 'm_user_menu_map';
				$where = ['id_user' => $this->input->post('userId'),'id_menu' => $this->input->post('menuId')];
				$insert = ['id_user' => $this->input->post('userId'),'id_menu' => $this->input->post('menuId'),'assign_by' => $currentUserLogin];
			}else if($mapping == 'submenu'){
				$table = 'm_user_submenu_map';
				$where = ['id_user' => $this->input->post('userId'),'id_menu' => $this->input->post('menuId'),'id_submenu' => $this->input->post('submenuId')];
				$insert = ['id_user' => $this->input->post('userId'),'id_menu' => $this->input->post('menuId'),'id_submenu' => $this->input->post('submenuId'),'assign_by' => $currentUserLogin];
			}

			$roleExist = $this->crud->readDataRow('*',$table,$where,[],'','','','');
			if(!$roleExist){
				$this->crud->createData($table,$insert);
			}else{
				$this->crud->deleteData($table,$where);
			}
			$return = ['status' => true,'message' => 'Data sudah diupdate, silahkan logout dan login ulang untuk melihat perubahan!'];
		}
		echo json_encode($return);
	}

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		is_logged_in();
		$this->load->library(['Home_Library','form_validation','upload']);
		$this->load->helper('dev');
	}

    public function index()
    {
        $data['title'] = 'Menu Management';
        $data['breadcrumb'] = ['left' => 'Menu','right' => ['App','Master','Menu']];
        $this->home_library->main('menu/index',$data);
    }

    function getAllMenu()
	{
		$column_order = array('id');
		$column_search = array('id');
		$order = array('id');
		$list = $this->crud->getDatatable('*','m_user_menu',[],$column_search,$column_order,$order,[]);
		$data = array();
		foreach ($list as $row) {
			$color = $row->is_parent == '1' ? 'success':'info';
			$label = $row->is_parent == '1' ? 'Daftar Submenu':'Tidak ada Submenu';
			$button =  $row->is_parent == '1' ? '<a href="#" data-bs-toggle="modal" data-bs-target="#modalListSubmenu" data-id-menu="'.$row->id.'" onclick="setIdMenu('.$row->id.')"><span class="badge label-table bg-'.$color.'">'.$label.'</span></a>' : '<span class="badge label-table bg-'.$color.'">'.$label.'</span>';
			$sub_array = array();
			$sub_array[] = $row->name;
			$sub_array[] = $row->icon;
			$sub_array[] = $row->url;
			$sub_array[] = $button;
			$sub_array[] = $row->created_date;
			$data[] = $sub_array;
		}

		$output = array(
			'draw'	        => $_POST['draw'],
			'recordsTotal'	=> $this->crud->dataTableCount('m_user_menu',[],[]),
			'recordsFiltered'	=> $this->crud->dataTableFilter('*','m_user_menu',[],$column_search,$column_order,$order,[]),
			'data'		=> $data
		);
		echo json_encode($output);
	}

	function postMenu()
	{
		$this->form_validation->set_rules('nama_menu', 'Nama Menu', 'trim|required');
		$this->form_validation->set_rules('icon', 'Icon Menu', 'trim|required');
		$this->form_validation->set_rules('url', 'Url Menu', 'trim|required');
		
		if ($this->form_validation->run() == FALSE) {
			$return = ['status' => false, 'message' => validation_errors()];
		} else {
			$formInput = $this->input->post('form_input');
			if($formInput == 'menu'){
				$dataInsert = [
					'name' => $this->input->post('nama_menu'),
					'icon' => $this->input->post('icon'),
					'url' => $this->input->post('url'),
					'is_parent' => $this->input->post('is_parent') ? '1':'0',
					'created_by' => $this->session->userdata('user_detail')['user_id']
				];
				$this->crud->createData('m_user_menu',$dataInsert);
			}else if($formInput == 'submenu'){
				$dataInsert = [
					'id_menu' => $this->input->post('id_menu'),
					'name' => $this->input->post('nama_menu'),
					'icon' => $this->input->post('icon'),
					'url' => $this->input->post('url'),
					'is_parent' => $this->input->post('is_parent') ? '1':'0',
					'created_by' => $this->session->userdata('user_detail')['user_id']
				];
				$this->crud->createData('m_user_submenu',$dataInsert);
			}
			$return = ['status' => true,'message' => 'Data Disimpan!','data' => $dataInsert];
		}
		echo json_encode($return);
	}

	function getSubMenu()
	{
		$id_menu = $this->input->post('id_menu');
		$column_order = array('id');
		$column_search = array('id');
		$order = array('id');
		$list = $this->crud->getDatatable('*','m_user_submenu',['id_menu'=>$id_menu],$column_search,$column_order,$order,[]);
		$data = array();
		foreach ($list as $row) {
			$color = $row->is_parent == '1' ? 'success':'info';
			$label = $row->is_parent == '1' ? 'Daftar Submenu':'Tidak ada Submenu';
			$button =  $row->is_parent == '1' ? '<a href="#" data-bs-toggle="modal" data-bs-target="#modalListSubmenu"><span class="badge label-table bg-'.$color.'">'.$label.'</span></a>' : '<span class="badge label-table bg-'.$color.'">'.$label.'</span>';
			$sub_array = array();
			$sub_array[] = $row->name;
			$sub_array[] = $row->icon;
			$sub_array[] = $row->url;
			$sub_array[] = $button;
			$sub_array[] = $row->created_date;
			$data[] = $sub_array;
		}

		$output = array(
			'draw'	        => $_POST['draw'],
			'recordsTotal'	=> $this->crud->dataTableCount('m_user_submenu',['id_menu'=>$id_menu],[]),
			'recordsFiltered'	=> $this->crud->dataTableFilter('*','m_user_submenu',['id_menu'=>$id_menu],$column_search,$column_order,$order,[]),
			'data'		=> $data
		);
		echo json_encode($output);
	}

}
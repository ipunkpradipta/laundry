<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customer extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		is_logged_in();
		$this->load->library(['Home_Library','form_validation','upload']);
		$this->load->helper('dev');
	}

    public function index()
    {
        $data['title'] = 'Konsumen';
        $data['breadcrumb'] = ['left' => 'Konsumen','right' => ['App','Master','Konsumen']];
        $this->home_library->main('customer/index',$data);
    }

	function getAllCustomer()
	{
		$column_order = array('id');
		$column_search = array('id');
		$order = array('id');
		$list = $this->crud->getDatatable('*','m_customer',[],$column_search,$column_order,$order,[]);
		$data = array();
		foreach ($list as $row) {
			$sub_array = array();
			$sub_array[] = $row->nama;
			$sub_array[] = $row->no_handphone;
			$sub_array[] = $row->alamat;
			$sub_array[] = $row->created_date;
			$data[] = $sub_array;
		}

		$output = array(
			'draw'	        => $_POST['draw'],
			'recordsTotal'	=> $this->crud->dataTableCount('m_customer',[],[]),
			'recordsFiltered'	=> $this->crud->dataTableFilter('*','m_customer',[],$column_search,$column_order,$order,[]),
			'data'		=> $data
		);
		echo json_encode($output);
	}

	public function postCustomer()
	{
		$no_handphone = $this->input->post('no_handphone');
		$this->form_validation->set_rules('nama', 'Nama Konsumen', 'trim|required');
		$this->form_validation->set_rules('no_handphone', 'Nomor HP', 'trim|required|min_length[12]|max_length[13]|is_unique[m_customer.no_handphone]',array('is_unique' => "Data dengan nomor HP $no_handphone sudah terdaftar"));
		$this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
		
		
		if ($this->form_validation->run() == FALSE) {
			$return = ['status' => false, 'message' => validation_errors()];
		} else {
			//Chekc if exist
			$check = $this->crud->readDataRow('*','m_customer',['no_handphone' => $no_handphone],[],'','','','');
			if($check){
				$return = ['status' => false, 'message' => "Data dengan nomor HP $no_handphone sudah terdaftar"];
			}else{
				$insert = [
					'nama' 			=> $this->input->post('nama'),
					'no_handphone' 	=> $this->input->post('no_handphone'),
					'alamat'		=> $this->input->post('alamat')
				];
				$this->crud->createData('m_customer',$insert);
				$return = ['status' => true, 'message' => "Data berhasil disimpan!"];
			}
		}
		echo json_encode($return);
	}

}
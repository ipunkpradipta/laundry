<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		is_logged_in();
		$this->load->library(['Home_Library','form_validation','upload']);
		$this->load->helper('dev');
	}

    public function index()
    {
        $data['title'] = 'Produk';
        $data['durations'] = $this->crud->readDataObject('*','m_product_duration',[],[],'','','','');
        $data['breadcrumb'] = ['left' => 'Produk','right' => ['App','Master','Produk']];
        $this->home_library->main('product/index',$data);
    }

	public function getAllProduct()
	{
		$durations = $this->crud->readDataObject('duration_in_day,nama','m_product_duration',[],[],'','','','');
		$column_order = array('id');
		$column_search = array('id');
		$order = array('id');
		$list = $this->crud->getDatatable('*','m_product',[],$column_search,$column_order,$order,[]);
		$data = array();
		foreach ($list as $row) {
			//Label Status Aktif
			$color = $row->status_aktif == '1' ? 'success':'info';
			$label = $row->status_aktif == '1' ? 'Aktif':'Tidak Aktif';
			$button =  $row->status_aktif == '1' ? '<span class="badge label-table bg-'.$color.'">'.$label.'</span>' : '<span class="badge label-table bg-'.$color.'">'.$label.'</span>';

			// Label Discount
			$discountLabel = ($row->diskon_item !== '0') ? '<span class="badge label-table bg-danger">Discount '.$row->diskon_item.'%</span>' : '';

			$sub_array = array();
			$sub_array[] = $row->nama_produk .' - '. $durations[array_search($row->durasi_pengerjaan,array_column($durations,'duration_in_day'))]->nama . ' ' . $discountLabel;
			$sub_array[] = $row->satuan;
			$sub_array[] = ($row->diskon_item !== '0')?'<div>
								<div class="col-md-6" style="">'.convertToRupiah($row->harga - ($row->diskon_item/100)*$row->harga).'</div>
								<div class="col-md-6 text-danger" style="font-size:12px;text-decoration:line-through;color:#f1556c">'.convertToRupiah($row->harga).'</div>
							</div>' : convertToRupiah($row->harga);
			$sub_array[] = $button;
			$sub_array[] = $row->created_date;
			$sub_array[] ='<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
								<div class="btn-group btn-group-sm" style="float: none;">
									<button type="button" class="tabledit-edit-button btn btn-success" style="float: none;">
										<span class="mdi mdi-pencil"></span>
									</button>
								</div>
							</div>';
			$data[] = $sub_array;
		}

		$output = array(
			'draw'	        => $_POST['draw'],
			'recordsTotal'	=> $this->crud->dataTableCount('m_product',[],[]),
			'recordsFiltered'	=> $this->crud->dataTableFilter('*','m_product',[],$column_search,$column_order,$order,[]),
			'data'		=> $data
		);
		echo json_encode($output);
	}

	public function postProduct()
	{
		$this->form_validation->set_rules('nama_produk', 'Nama Produk', 'trim|required');
		$this->form_validation->set_rules('satuan', 'Satuan', 'trim|required');
		$this->form_validation->set_rules('duration', 'Durasi Pengerjaan', 'trim|required');
		$this->form_validation->set_rules('diskon_item', 'Durasi Pengerjaan', 'trim|required');
		$this->form_validation->set_rules('harga', 'Harga', 'trim|required');
		
		if ($this->form_validation->run() == FALSE) {
			$return = ['status' => false, 'message' => validation_errors()];
		} else {
			$arrayInsert= [
				'nama_produk'		=> strtoupper($this->input->post('nama_produk')),
				'satuan' 			=> $this->input->post('satuan'),
				'harga' 			=> preg_replace('/[^0-9]/', '', $this->input->post('harga')),
				'diskon_item' 		=> $this->input->post('diskon_item'),
				'durasi_pengerjaan' => $this->input->post('duration'),
				'created_by' 		=> $this->session->userdata('user_detail')['user_id']
			];
			// Insert into database
			$this->crud->createData('m_product',$arrayInsert);
			$return = ['status' => true, 'message' => 'Produk disimpan!','data' => $arrayInsert];
		}
		
		echo json_encode($return);
	}

}
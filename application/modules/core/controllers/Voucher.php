<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Voucher extends CI_Controller
{
    
    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }
    

    public function index()
    {
        $data['title'] = 'Daftar Voucher';
        $data['voucher'] = generateRandomString(6);
        $data['breadcrumb'] = ['left' => 'Daftar Voucher','right' => ['App','Voucher','Daftar Voucher']];
        $this->home_library->main('voucher/index',$data);
    }

    public function create()
    {
        $data['title'] = 'Buat Voucher';
        $data['breadcrumb'] = ['left' => 'Buat Voucher','right' => ['App','Voucher','Buat Voucher']];
        $this->home_library->main('voucher/create',$data);
    }

    public function getAllVoucher()
    {
        $column_order = array('id');
		$column_search = array('id');
		$order = array('id');
		$list = $this->crud->getDatatable('*','d_voucher',[],$column_search,$column_order,$order,[]);

		$data = array();
		foreach ($list as $row) {
			$sub_array = array();
			$sub_array[] = $row->kode_voucher;
			$sub_array[] = $row->status_used;
			$sub_array[] = $row->valid_date_start;
			$sub_array[] = $row->valid_date_end;
			$sub_array[] = $row->cut_value;
			$sub_array[] = $row->cut_type;
			$sub_array[] = $row->cut_max_value;
			// $sub_array[] = convertToRupiah((float)$row->harga);
			// $sub_array[] = convertToRupiah($total);
			// $sub_array[] ='<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
			// 					<div class="btn-group btn-group-sm" style="float: none;">
			// 						<button type="button" onclick="deleteItemCart('.$row->id.')" class="tabledit-delete-button btn btn-danger" style="float: none;">
			// 							<span class="mdi mdi-close"></span>
			// 						</button>
			// 					</div>
			// 				</div>';
			$data[] = $sub_array;
		}
        $output = array(
			'draw'	        => $_POST['draw'],
			'recordsTotal'	=> $this->crud->dataTableCount('d_voucher',[],[]),
			'recordsFiltered'	=> $this->crud->dataTableFilter('*','d_voucher',[],$column_search,$column_order,$order,[]),
			'data'		=> $data,
		);
		echo json_encode($output);
    }

	public function postVoucher()
	{
		$this->form_validation->set_rules('valid_date_start', 'Berlaku Mulai', 'trim|required');
		$this->form_validation->set_rules('valid_date_end', 'Berlaku Sampai', 'trim|required');
		$this->form_validation->set_rules('cut_value', 'Potongan', 'trim|required');
		$this->form_validation->set_rules('cut_type', 'Bentuk Potongan', 'trim|required');
		$this->form_validation->set_rules('cut_max_value', 'Maksimal Potongan', 'trim|required');
		
		
		if ($this->form_validation->run() == FALSE) {
			$return = ['status' => false, 'message' => validation_errors()];
		} else {
			$insertVoucher = [
				'kode_voucher' => generateRandomString(6),
				'valid_date_start' => $this->input->post('valid_date_start'),
				'valid_date_end' => $this->input->post('valid_date_end'),
				'cut_value' => $this->input->post('cut_value'),
				'cut_type' => $this->input->post('cut_type'),
				'cut_max_value' => $this->input->post('cut_max_value'),
				'flag_max_value' => !empty($this->input->post('cut_max_value'))? '1' : '0',
				'created_by' => $this->session->userdata('user_detail')['user_id']
			];
			$this->crud->createData('d_voucher',$insertVoucher);
			$return = ['status' => true, 'message' => 'Voucher berhasil dibuat', 'data' => $insertVoucher];
		}
		echo json_encode($return);
	}
}
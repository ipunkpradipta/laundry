<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Nota extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        //Do your magic here
    }


    public function index()
    {
        $data['title'] = 'Daftar Nota';
        $data['breadcrumb'] = ['left' => 'Daftar Nota', 'right' => ['App', 'Nota', 'Daftar Nota']];
        $this->home_library->main('nota/index', $data);
    }

    public function create()
    {
        $data['title'] = 'Buat Nota';
        $data['breadcrumb'] = ['left' => 'Buat Nota', 'right' => ['App', 'Nota', 'Buat Nota']];
        $this->home_library->main('nota/create', $data);
    }

    public function getAllCustomer()
    {
        $query = $this->input->get('query');
        $customerListObject = $this->crud->readDataObject('no_handphone,nama,id', 'm_customer', ['no_handphone LIKE "%' . $query . '%"' => null], [], '', '', '', '');
        $customerList['query'] = $query;
        $customerList['suggestions'] = [];
        foreach ($customerListObject as $customer) {
            $customerList['suggestions'][] = [
                'value' => $customer->no_handphone . '-' . $customer->nama,
                'data' => $customer->id
            ];
        }
        echo json_encode($customerList);
    }

    public function getAllProducts()
    {
        $query = $this->input->get('query');
        $joinTable[0]['table'] = 'm_product_duration b';
        $joinTable[0]['relation'] = 'a.durasi_pengerjaan = b.duration_in_day';
        $customerListObject = $this->crud->readDataObject('a.id,a.nama_produk,b.nama,a.harga,a.diskon_item', 'm_product a', ['nama_produk LIKE "%' . $query . '%"' => null, 'status_aktif' => '1'], $joinTable, 'LEFT', '', '', '');
        $customerList['query'] = $query;
        $customerList['suggestions'] = [];
        foreach ($customerListObject as $customer) {
            $harga = $customer->harga;
            $label = '';
            if ($customer->diskon_item !== '0') {
                $harga = $customer->harga - ($customer->diskon_item / 100) * $customer->harga;
                $label = " - Discount ( $customer->diskon_item% )";
            }
            $customerList['suggestions'][] = [
                'value' => $customer->nama_produk . ' - ' . $customer->nama . ' - ' . convertToRupiah($customer->harga) . $label,
                'data' => [
                    'id' => $customer->id,
                    'harga' => convertToRupiah($harga)
                ]
            ];
        }
        echo json_encode($customerList);
    }

    public function getAllCart()
    {
        $id_user = $this->session->userdata('user_detail')['user_id'];
        $sumTotal = 0;
        $grandTotal = 0;
        //validasi voucher code
        $voucher_code = $_GET['voucherCode'];
        // $discount = $this->_validationVoucherCode($voucher_code);
        $discount = isset($_GET['value']) ? $_GET['value'] : 0;
        // $discount = 0;

        $column_order = array('id');
        $column_search = array('id');
        $order = array('id');
        $list = $this->crud->getDatatable('*', 'temp_nota', ['created_by' => $id_user], $column_search, $column_order, $order, []);
        $data = array();
        foreach ($list as $row) {
            $total = ((float)$row->harga * (float)$row->banyaknya);
            $sub_array = array();
            $sub_array[] = $row->nama_product;
            $sub_array[] = $row->total_item;
            $sub_array[] = $row->banyaknya;
            $sub_array[] = $row->satuan;
            $sub_array[] = convertToRupiah((float)$row->harga);
            $sub_array[] = convertToRupiah($total);
            $sub_array[] = '<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
								<div class="btn-group btn-group-sm" style="float: none;">
									<button type="button" onclick="deleteItemCart(' . $row->id . ')" class="tabledit-delete-button btn btn-danger" style="float: none;">
										<span class="mdi mdi-close"></span>
									</button>
								</div>
							</div>';
            $sub_array[] = $row->id;
            $data[] = $sub_array;
            $grandTotal += $total;
        }
        $sumTotal = $grandTotal;
        $grandTotal = $grandTotal - $discount;
        $output = array(
            'draw'            => $_POST['draw'],
            'recordsTotal'    => $this->crud->dataTableCount('temp_nota', ['created_by' => $id_user], []),
            'recordsFiltered'    => $this->crud->dataTableFilter('*', 'temp_nota', ['created_by' => $id_user], $column_search, $column_order, $order, []),
            'data'        => $data,
            'voucher_code' => $voucher_code,
            'voucher_discount' => convertToRupiah($discount),
            'sumTotal' => convertToRupiah($sumTotal),
            'grandTotal' => convertToRupiah($grandTotal)
        );
        echo json_encode($output);
    }

    public function postCart()
    {
        $this->form_validation->set_rules('nama_produk', 'Nama Product', 'trim|required');
        $this->form_validation->set_rules('id_produk', 'Id', 'trim|required');
        $this->form_validation->set_rules('total_item', 'Item', 'trim|required');
        $this->form_validation->set_rules('banyaknya', 'Banyak', 'trim|required');
        $this->form_validation->set_rules('satuan', 'Satuan', 'trim|required');
        $this->form_validation->set_rules('harga_produk', 'Harga', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $return = ['status' => false, 'message' => validation_errors()];
        } else {
            $arrayInsertCart = [
                'id_product' => $this->input->post('id_produk'),
                'nama_product' => $this->input->post('nama_produk'),
                'total_item' => $this->input->post('total_item'),
                'banyaknya' => $this->input->post('banyaknya'),
                'satuan' => $this->input->post('satuan'),
                'harga' => preg_replace('/[^0-9]/', '', $this->input->post('harga_produk')),
                'created_by' => $this->session->userdata('user_detail')['user_id']
            ];

            $this->crud->createData('temp_nota', $arrayInsertCart);

            $return = ['status' => true, 'message' => 'Disimpan ke keranjang!'];
        }

        echo json_encode($return);
    }

    public function deleteCart()
    {
        $this->form_validation->set_rules('id', 'Id Data', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $return  = ['status' => false, 'message' => validation_errors()];
        } else {
            $this->crud->deleteData('temp_nota', ['id' => $this->input->post('id')]);
            $return = ['status' => true, 'message' => 'Berhasil hapus item'];
        }
        echo json_encode($return);
    }

    public function validationVoucherCode()
    {
        $voucherCode = $this->input->post('voucher_code');
        $totalTagihan = $this->input->post('totalTagihan');
        $result = $this->crud->readDataRow('*', 'd_voucher', ['kode_voucher' => $voucherCode], [], '', '', '', '');
        if ($result) {
            //Checking expired
            $return = getValueFromVoucherCode($totalTagihan, $result);
            $return['voucher_code'] = $voucherCode;
        } else {
            $return = ['status' => false, 'message' => 'Voucher tidak ditemukan!', 'value' => 0];
        }
        echo json_encode($return);
    }

    public function createNota()
    {
        $this->form_validation->set_rules('nama_konsumen', 'Nama Konsumen', 'trim|required');
        $this->form_validation->set_rules('no_handphone', 'No Handphone', 'trim|required');
        $this->form_validation->set_rules('tagihan', 'Tagihan', 'trim|required');
        $this->form_validation->set_rules('ditagihkan', 'Ditagihkan', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $return = ['status' => false, 'message' => validation_errors()];
        } else {
            if (empty($this->input->post('id_konsumen'))) {
                //insert new customer
                $insertCustomer = [
                    'nama' => $this->input->post('nama_konsumen'),
                    'no_handphone' => $this->input->post('no_handphone'),
                    'created_by' => $this->session->userdata('user_detail')['user_id'],
                ];
                $id_customer = $this->crud->createDataReturnId('m_customer', $insertCustomer);
            }
            $no_nota = generateNewNota();
            $array_insert = [
                'no_nota' => $no_nota,
                'tgl_nota' => date('Y-m-d'),
                'id_customer' => $this->input->post('id_konsumen') ? $this->input->post('id_konsumen') : $id_customer,
                'jumlah_tagihan' => preg_replace('/[^0-9]/', '', $this->input->post('tagihan')),
                'jumlah_potongan' => preg_replace('/[^0-9]/', '', $this->input->post('voucher')),
                'jumlah_ditagihkan' => preg_replace('/[^0-9]/', '', $this->input->post('ditagihkan')),
                // 'tenggat_waktu_proses' => $this->input->post('tagihan'),
                // 'status_proses_terakhir' => $this->input->post('tagihan'),
                'created_by' => $this->session->userdata('user_detail')['user_id'],
            ];
            $id_header_nota = $this->crud->createDataReturnId('d_nota_header', $array_insert);
            if ($id_header_nota) {
                $tenggat_waktu = 0;
                foreach ($this->input->post('idKeranjang') as $key) {

                    $data_cart = $this->crud->readDataRow('*', 'temp_nota', ['id' => $key], [], '', '', '', '');
                    $insertDetail = [
                        'id_header' => $id_header_nota,
                        'id_product' => $data_cart->id_product,
                        'total_item' => $data_cart->total_item,
                        'banyaknya' => $data_cart->banyaknya,
                        'satuan' => $data_cart->satuan,
                        'harga_satuan' => $data_cart->harga,
                        'harga_total' => (float) $data_cart->harga * (float) $data_cart->banyaknya,
                        'created_by' => $this->session->userdata('user_detail')['user_id'],
                    ];
                    $this->crud->createData('d_nota_detail', $insertDetail);
                    //if success insert delete in temp table
                    $this->crud->deleteData('temp_nota', ['id' => $key]);

                    $data_product = $this->crud->readDataRow('*', 'm_product', ['id' => $data_cart->id_product], [], '', '', '', '');
                    if ($tenggat_waktu < $data_product->durasi_pengerjaan) {
                        $tenggat_waktu = $data_product->durasi_pengerjaan;
                    }
                }
                $time_sekarang = time();
                $waktu = date("Y-m-d H:i:s", strtotime("+$tenggat_waktu days", $time_sekarang));
                $this->crud->updateData('d_nota_header', ['tenggat_waktu_proses' => $waktu], ['id' => $id_header_nota]);
            }
            $return = ['status' => true, 'message' => 'Nota disimpan!', 'no_nota' => $no_nota];
        }

        echo json_encode($return);
    }

    public function getAllNota()
    {
        $column_order = array('id');
        $column_search = array('id');
        $order = array('id');
        $list = $this->crud->getDatatable('*', 'd_nota_header', [], $column_search, $column_order, $order, []);
        $data = array();
        foreach ($list as $row) {
            $begin = new DateTime(date('Y-m-d H:i:s'));
            $end = new DateTime($row->tenggat_waktu_proses);
            $diff = $begin->diff($end);
            $sub_array = array();
            $sub_array[] = $row->no_nota;
            $sub_array[] = $row->tgl_nota;
            $sub_array[] = $row->id_customer;
            $sub_array[] = convertToRupiah($row->jumlah_tagihan);
            $sub_array[] = convertToRupiah($row->jumlah_potongan);
            $sub_array[] = convertToRupiah($row->jumlah_ditagihkan);
            $sub_array[] = $row->tenggat_waktu_proses . ' <span class="badge label-table bg-danger">Sisa ' . $diff->format("%d") . ' Hari</span>';
            $sub_array[] = $row->status_proses_terakhir;
            $sub_array[] = $row->created_by;
            $sub_array[] = $row->created_date;
            $sub_array[] = '<div class="tabledit-toolbar btn-toolbar" style="text-align: left;">
								<div class="btn-group btn-group-sm" style="float: none;">
									<button type="button" onclick="detailNotaByid(' . $row->id . ')" class="tabledit-delete-button btn btn-warning" style="float: none;">
										Detail
									</button>
								</div>
							</div>';
            $data[] = $sub_array;
        }
        $output = array(
            'draw'            => $_POST['draw'],
            'recordsTotal'    => $this->crud->dataTableCount('d_nota_header', [], []),
            'recordsFiltered' => $this->crud->dataTableFilter('*', 'd_nota_header', [], $column_search, $column_order, $order, []),
            'data'            => $data
        );
        echo json_encode($output);
    }

    public function getNotaById()
    {
        $durations = $this->crud->readDataObject('duration_in_day,nama', 'm_product_duration', [], [], '', '', '', '');
        $this->form_validation->set_rules('id', 'Id', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            $return = ['status' => false, 'message' => validation_errors()];
        } else {
            $data = [];
            $prepareData = $this->crud->readDataObject('*', 'd_nota_header', ['id' => $this->input->post('id')], [], '', '', '', '');
            if (count($prepareData) > 0) {
                foreach ($prepareData as $header) {
                    $data = [
                        'no_nota' => $header->no_nota,
                        'tgl_nota' => $header->tgl_nota,
                        'created_date' => $header->created_date,
                        'customer_name' => $this->crud->readDataRow('nama', 'm_customer', ['id' => $header->id_customer], [], '', '', '', '')->nama,
                        'status_proses_terakhir' => $header->status_proses_terakhir,
                        'jumlah_tagihan' => convertToRupiah($header->jumlah_tagihan),
                        'jumlah_potongan' => convertToRupiah($header->jumlah_potongan),
                        'jumlah_ditagihkan' => convertToRupiah($header->jumlah_ditagihkan)
                    ];
                    // $data = (array) $header;
                    $items = $this->crud->readDataObject('*', 'd_nota_detail', ['id_header' => $header->id], [], '', '', '', '');
                    foreach ($items as $item) {
                        $data_product = $this->crud->readDataRow('*', 'm_product', ['id' => $item->id_product], [], '', '', '', '');
                        $data['item'][] = [
                            'product_name' => $data_product->nama_produk . ' - ' . $durations[array_search($data_product->durasi_pengerjaan, array_column($durations, 'duration_in_day'))]->nama,
                            'total_item' => $item->total_item,
                            'banyaknya' => $item->banyaknya . $item->satuan,
                            'harga_satuan' => convertToRupiah($item->harga_satuan),
                            'harga_total' => convertToRupiah($item->harga_total),
                        ];
                    }
                }
            }
            $return = ['status' => true, 'message' => 'Berhasil get data by id', 'data' => $data];
        }
        echo json_encode($return);
    }
}

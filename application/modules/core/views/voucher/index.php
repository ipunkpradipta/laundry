<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <div class="row mb-2">
          <div class="col-sm-4">
            <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#modalCreateVoucher" class="btn btn-danger mb-2">
              <i class="mdi mdi-plus-circle me-2"></i> 
              Buat Voucher
            </a>
          </div>
        </div>
        <table id="table_voucher" class="table dt-responsive nowrap w-100">
          <thead class="table-light">
            <tr>
              <th rowspan="2" class="align-middle">Kode</th>
              <th rowspan="2" class="align-middle">Status Dipakai</th>
              <th colspan="2" class="text-center">Berlaku</th>
              <th colspan="4" class="text-center">Potongan</th>
            </tr>
            <tr>
              <th>Mulai</th>
              <th>Selesai</th>
              <th>Nilai</th>
              <th>Tipe</th>
              <th>Maksimal</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalCreateVoucher" tabindex="-1" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="myCenterModalLabel">Form Input</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="#" method="post" id="form_voucher_add">
          <div class="form-floating mb-3">
            <input type="text" class="form-control" id="valid_date_start" name="valid_date_start" placeholder="Menu" autocomplete="off">
            <label for="valid_date_start">Berlaku Mulai</label>
          </div>
          <div class="form-floating mb-3">
            <input type="text" class="form-control" id="valid_date_end" name="valid_date_end" placeholder="SVG valid_date_end" autocomplete="off">
            <label for="valid_date_end">Berlaku Sampai</label>
          </div>
          <div class="form-floating mb-3">
            <input type="text" class="form-control" id="cut_value" name="cut_value" placeholder="#" autocomplete="off">
            <label for="cut_value">Potongan</label>
          </div>
          <div class="form-floating mb-3">
            <select name="cut_type" id="cut_type" class="form-control">
              <option value="persen">Persen(%)</option>
              <option value="harga">Harga</option>
              <option value="berat">Berat</option>
            </select>
            <label for="cut_type">Bentuk Potongan</label>
          </div>
          <div class="form-floating mb-3">
            <input type="text" class="form-control" id="cut_max_value" name="cut_max_value" placeholder="#" autocomplete="off">
            <label for="cut_max_value">Maksimal Potongan</label>
          </div>
          <button type="submit" id="menu-submit-btn" class="btn btn-warning waves-effect waves-light" data-toggle="reload">Submit</button>
          <button class="btn btn-warning" id="btn-spinner" type="button" disabled="" style="display:none">
            <span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>
            Loading...
          </button>
        </form>
      </div>
    </div>
  </div>
</div>
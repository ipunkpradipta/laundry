<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <form action="#" id="form_nota_customer">
          <!-- <div class="row">
            <div class="col-md">
              <h3>Nomor Nota : XXX-OI-00001</h3>
              <input type="hidden" name="no_nota" id="no_nota" value="XXX-OI-00001">
            </div>
          </div> -->
          <div class="row">
            <div class="col-md-3">
              <div class="form-floating mb-2">
                <input type="text" class="form-control" id="no_handphone" name="no_handphone" placeholder="Alexandre Christie" autocomplete="off">
                <label for="no_handphone">No Hp</label>
              </div>
            </div>
            <div class="col-md-3">
              <div class="form-floating mb-2">
                <input type="text" class="form-control" id="nama_konsumen" name="nama_konsumen" placeholder="Alexandre Christie" autocomplete="off">
                <label for="nama_konsumen">Nama Konsumen</label>
              </div>
              <input type="hidden" name="id_konsumen" id="id_konsumen">
            </div>
          </div>
        </form>
        <form action="#" method="post" id="form_nota_add_cart">
          <div class="row">
            <div class="col-md-10">
              <div class="form-floating mb-2">
                <input type="text" class="form-control" id="nama_produk" name="nama_produk" placeholder="Alexandre Christie" autocomplete="off">
                <label for="nama_produk">Pilih Product</label>
                <input type="hidden" class="form-control" id="id_produk" name="id_produk" placeholder="Alexandre Christie" autocomplete="off">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-2">
              <div class="form-floating mb-2">
                <input type="text" class="form-control" id="harga_produk" name="harga_produk" placeholder="Alexandre Christie" autocomplete="off">
                <label for="harga_produk">Harga</label>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-floating mb-2">
                <input type="text" class="form-control" id="total_item" name="total_item" placeholder="Alexandre Christie" autocomplete="off">
                <label for="total_item">Total Item</label>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-floating mb-2">
                <input type="text" class="form-control" id="banyaknya" name="banyaknya" placeholder="Alexandre Christie" autocomplete="off">
                <label for="banyaknya">Banyaknya</label>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-floating mb-2">
                <select name="satuan" id="satuan" class="form-control">
                  <option value="KG">KG</option>
                  <option value="PCS">PCS</option>
                </select>
                <label for="nama_konsumen">Satuan</label>
              </div>
            </div>
            <div class="col-md-2">
              <button type="submit" id="menu-submit-btn" class="btn btn-warning waves-effect waves-light" data-toggle="reload">Tambah Keranjang</button>
              <button class="btn btn-warning" id="btn-spinner" type="button" disabled="" style="display:none">
                <span class="spinner-border spinner-border-sm me-1" role="status" aria-hidden="true"></span>
                Loading...
              </button>
            </div>
          </div>
        </form>
        <div class="row mt-2">
          <h4>Keranjang</h4>
        </div>
        <table id="table_cart" class="table dt-responsive nowrap w-100">
          <thead>
            <tr>
              <th>Produk</th>
              <th>Total Item</th>
              <th>Banyaknya</th>
              <th>Satuan</th>
              <th>Harga</th>
              <th>Total</th>
              <th>#</th>
            </tr>
          </thead>
        </table>
      </div> <!-- end card body-->
    </div> <!-- end card -->
  </div><!-- end col-->
</div>
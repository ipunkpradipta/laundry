<style>
  .kiri {
    display: flex;
    justify-content: start;
    align-items: center;
  }

  .kanan {
    display: flex;
    justify-content: end;
    align-items: center;
  }
</style>
<div class="row">
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <table id="table_nota" class="table dt-responsive nowrap w-100">
          <thead>
            <tr>
              <th>Nota</th>
              <th>Tanggal Nota</th>
              <th>Konsumen</th>
              <th>Jumlah Tagihan</th>
              <th>Jumlah Potongan</th>
              <th>Jumlah Ditagihkan</th>
              <th>Estimasi</th>
              <th>Status</th>
              <th>Dibuat</th>
              <th>Tanggal</th>
              <th>#</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>

<div id="full-width-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="fullWidthModalLabel" aria-hidden="true">
  <!-- <div class="modal-dialog modal-full-width"> -->
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="fullWidthModalLabel">Modal Heading</h4>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="col-md-12 modal_body">
          <div class="row">
            <div class="col-md-6 kiri">Status</div>
            <div class="col-md-6 kanan status_nota">2021-10-11</div>
          </div>
          <div class="row">
            <div class="col-md-6 kiri">Tanggal Nota</div>
            <div class="col-md-6 kanan tgl_nota">2021-10-11</div>
          </div>
          <div class="row">
            <div class="col-md-6 kiri">Tanggal Transaksi</div>
            <div class="col-md-6 kanan tgl_trx">2021-10-11 17:05:45</div>
          </div>
          <div class="row">
            <div class="col-md-6 kiri">Customer Name</div>
            <div class="col-md-6 kanan customer_name">ABC.com</div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-6 kiri">Jumlah Tagihan</div>
            <div class="col-md-6 kanan jumlah_tagihan">Rp. 56.000</div>
          </div>
          <div class="row">
            <div class="col-md-6 kiri">Potongan</div>
            <div class="col-md-6 kanan jumlah_potongan">Rp. 0</div>
          </div>
          <div class="row">
            <div class="col-md-6 kiri">Jumlah Ditagihkan</div>
            <div class="col-md-6 kanan jumlah_ditagihkan">Rp. 56.000</div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-12">
              <table class="table table-borderless">
                <thead>
                  <tr>
                    <th>Deskripsi</th>
                    <th>Item</th>
                    <th>Jumlah</th>
                    <th>Harga</th>
                    <th>Total</th>
                  </tr>
                </thead>
                <tbody class="body_item">
                  <tr>
                    <td>CUCI LIPAT</td>
                    <td>34</td>
                    <td>5 KG</td>
                    <td>Rp. 9000</td>
                    <td>Rp. 45000</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div>
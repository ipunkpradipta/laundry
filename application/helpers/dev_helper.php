<?php

function is_logged_in()
{
    $ci = get_instance();
    $session = $ci->session->userdata('user_detail')['user_email'];
    // print_r($session);die;
    if (empty($session)) {
        redirect('auth');
    }
    // } else {
    //     // $role_id = $ci->session->userdata('role_id');
    //     // $menu = $ci->uri->segment(2);
    //     // // print_r($menu);die;
    //     // $queryMenu = $ci->db->get_where('user_menu', ['controller' => $menu])->row_array();
    //     // $menu_id = $queryMenu['id'];
    //     // $userAccess = $ci->db->get_where('user_access_menu', ['role_id' => $role_id, 'menu_id' => $menu_id]);
    //     // if ($userAccess->num_rows() < 1) {
    //     //     if($menu == ''){
    //     redirect('home/dashboard');
    //     //     }else{
    //     //         redirect('auth');
    //     //     }
    //     // }
    // }
}

function textToSlug($text = '')
{
    $text = trim($text);
    if (empty($text)) return '';
    $text = preg_replace("/[^a-zA-Z0-9\-\s]+/", "", $text);
    $text = strtolower(trim($text));
    $text = str_replace(' ', '-', $text);
    $text = $text_ori = preg_replace('/\-{2,}/', '-', $text);
    return $text;
}

function checkAccessMenu($user_id, $menu_id)
{
    $ci = get_instance();
    $result = $ci->db->get_where('m_user_menu_map', [
        'user_id' => $user_id,
        'menu_id' => $menu_id
    ]);

    if ($result->num_rows() > 0) {
        return "checked='checked'";
    }
}

function checkAccessSubmenu($user_id, $submenu)
{
    $ci = get_instance();
    $result = $ci->db->get_where('m_user_menu_map', [
        'user_id' => $user_id,
        'menu_id' => $submenu
    ]);

    if ($result->num_rows() > 0) {
        return "checked='checked'";
    }
}

function konversiBulan($angka)
{
    $bulanInggris = array('Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
    $index = $angka - 1;
    return $bulanInggris[$index];
}

function penyebut($nilai)
{
    $nilai = abs($nilai);
    $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
    $temp = "";
    if ($nilai < 12) {
        $temp = " " . $huruf[$nilai];
    } else if ($nilai < 20) {
        $temp = penyebut($nilai - 10) . " Belas";
    } else if ($nilai < 100) {
        $temp = penyebut($nilai / 10) . " Puluh" . penyebut($nilai % 10);
    } else if ($nilai < 200) {
        $temp = " Seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = penyebut($nilai / 100) . " Ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = " Seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = penyebut($nilai / 1000) . " Ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = penyebut($nilai / 1000000) . " Juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = penyebut($nilai / 1000000000) . " Milyar" . penyebut(fmod($nilai, 1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = penyebut($nilai / 1000000000000) . " Trilyun" . penyebut(fmod($nilai, 1000000000000));
    }
    return $temp;
}

function terbilang($nilai)
{
    if ($nilai < 0) {
        $hasil = "minus " . trim(penyebut($nilai));
    } else {
        $hasil = trim(penyebut($nilai));
    }
    return $hasil;
}

function timeFromExcelToLinux($excel_time)
{
    return date('Y-m-d', ($excel_time - 25569) * 86400);
}

function getBrowserName()
{
    $u_agent = $_SERVER['HTTP_USER_AGENT'];
    $bname = 'Unknown';
    $platform = 'Unknown';
    $version = "";

    //First get the platform?
    if (preg_match('/linux/i', $u_agent)) {
        $platform = 'linux';
    } elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
        $platform = 'mac';
    } elseif (preg_match('/windows|win32/i', $u_agent)) {
        $platform = 'windows';
    }

    // Next get the name of the useragent yes seperately and for good reason
    if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent)) {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    } elseif (preg_match('/Firefox/i', $u_agent)) {
        $bname = 'Mozilla Firefox';
        $ub = "Firefox";
    } elseif (preg_match('/OPR/i', $u_agent)) {
        $bname = 'Opera';
        $ub = "Opera";
    } elseif (preg_match('/Chrome/i', $u_agent) && !preg_match('/Edge/i', $u_agent)) {
        $bname = 'Google Chrome';
        $ub = "Chrome";
    } elseif (preg_match('/Safari/i', $u_agent) && !preg_match('/Edge/i', $u_agent)) {
        $bname = 'Apple Safari';
        $ub = "Safari";
    } elseif (preg_match('/Netscape/i', $u_agent)) {
        $bname = 'Netscape';
        $ub = "Netscape";
    } elseif (preg_match('/Edge/i', $u_agent)) {
        $bname = 'Edge';
        $ub = "Edge";
    } elseif (preg_match('/Trident/i', $u_agent)) {
        $bname = 'Internet Explorer';
        $ub = "MSIE";
    }

    // finally get the correct version number
    $known = array('Version', $ub, 'other');
    $pattern = '#(?<browser>' . join('|', $known) .
        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
    if (!preg_match_all($pattern, $u_agent, $matches)) {
        // we have no matching number just continue
    }
    // see how many we have
    $i = count($matches['browser']);
    if ($i != 1) {
        //we will have two since we are not using 'other' argument yet
        //see if version is before or after the name
        if (strripos($u_agent, "Version") < strripos($u_agent, $ub)) {
            $version = $matches['version'][0];
        } else {
            $version = $matches['version'][1];
        }
    } else {
        $version = $matches['version'][0];
    }

    // check if we have a number
    if ($version == null || $version == "") {
        $version = "?";
    }

    return array(
        'userAgent' => $u_agent,
        'name'      => $bname,
        'version'   => $version,
        'platform'  => $platform,
        'pattern'    => $pattern
    );
}

function convertToRupiah($angka)
{
    $hasil_rupiah = "Rp " . number_format($angka, 0, ',', '.');
    return $hasil_rupiah;
}

function generateRandomString($length = 10)
{
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function getValueFromVoucherCode($total_tagihan, $data)
{
    $startDate = strtotime($data->valid_date_start);
    $dueDate = strtotime($data->valid_date_end);
    $now = strtotime(date('Y-m-d'));
    if ($now < $startDate) {
        // voucher belum berlaku / belum bisa dipakai
        $return = ['status' => false, 'message' => 'Voucher belum bisa digunakan!', 'data' => $data, 'value' => 0];
    } else if ($now > $dueDate) {
        // voucher sudah expired 
        $return = ['status' => false, 'message' => 'Voucher sudah expired!', 'data' => $data, 'value' => 0];
    } else {
        $cut_type = $data->cut_type;
        $cut_value = $data->cut_value;
        if ($cut_type == 'persen') {
            $discountPersen = ((float) $total_tagihan / 100) * (float) $cut_value;
            if ($data->flag_max_value == '1') {
                if ($discountPersen > $data->cut_max_value) {
                    $discountValue = (float) $data->cut_max_value;
                } else {
                    $discountValue = $discountPersen;
                }
            } else {
                $discountValue = $discountPersen;
            }
        } else if ($cut_type == 'harga') {
            $discountValue = (float)$cut_value;
        }
        $return = [
            'status' => true,
            'message' => 'Voucher berhasil digunakan!',
            'data' => $data,
            'value' => $discountValue,
            'formated' => convertToRupiah($discountValue)
        ];
    }
    return $return;
}

function generateNewNota()
{
    $ci = get_instance();
    $thisMonth = date("My");
    $where = "no_nota LIKE '%$thisMonth%'";
    $prefix = 'IP';
    $queryLast = $ci->crud->readDataRow('no_nota', 'd_nota_header', [$where => null], [], '', '', 'no_nota', 'desc');
    if ($queryLast) {
        $lastNoNota = $queryLast->no_nota;
    } else {
        $lastNoNota = 'IP' . $thisMonth . '00000';
    }
    $lastNoUrut = substr($lastNoNota, 7, 5);
    $nextNoUrut = $lastNoUrut + 1;
    $nextNo = sprintf('%05s', $nextNoUrut);
    $nextNoNota = strtoupper($prefix . $thisMonth . $nextNo);
    return $nextNoNota;
}

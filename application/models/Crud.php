<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Crud extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	function createData($table, $data)
	{
		return $this->db->insert($table, $data);
		// return $this->db->error();
	}

	function createDataReturnId($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
		// return $this->db->error();
	}

	function createDataMultiple($table, $data)
	{
		return $this->db->insert_batch($table, $data);
		// return $this->db->error();
	}

	function readDataArray($select, $from, $where, $joinTable, $joinTableType = 'LEFT', $groupBy, $order, $orderBy, $limit = null)
	{
		$this->db->select('SQL_CALC_FOUND_ROWS ' . $select, false);
		$this->db->from($from);
		if (count((array)$joinTable)) {
			foreach ($joinTable as $join) {
				$this->db->join($join['table'], $join['relation'], $joinTableType);
			}
		}
		$this->db->where($where);
		if ($groupBy != '') {
			$this->db->group_by($groupBy);
		}
		$this->db->order_by($order, $orderBy);
		if (!is_null($limit)) $this->db->limit($limit[0], $limit[1]);
		$query = $this->db->get();
		return $query->result_array();
		//return $this->db->last_query();
	}

	function readDataObject($select, $from, $where, $joinTable, $joinTableType = 'LEFT', $groupBy, $order, $orderBy, $limit = null)
	{
		$this->db->select('SQL_CALC_FOUND_ROWS ' . $select, false);
		$this->db->from($from);
		if (count((array)$joinTable)) {
			foreach ($joinTable as $join) {
				$this->db->join($join['table'], $join['relation'], $joinTableType);
			}
		}
		$this->db->where($where);
		if ($groupBy != '') {
			$this->db->group_by($groupBy);
		}
		$this->db->order_by($order, $orderBy);
		if (!is_null($limit)) $this->db->limit($limit[0], $limit[1]);
		$query = $this->db->get();
		return $query->result();
		//return $this->db->last_query();
	}

	function readDataCount($select, $from, $where, $joinTable, $joinTableType = 'LEFT', $groupBy, $order, $orderBy, $limit = null)
	{
		$this->db->select('SQL_CALC_FOUND_ROWS ' . $select, false);
		$this->db->from($from);
		if (count((array) $joinTable)) {
			foreach ($joinTable as $join) {
				$this->db->join($join['table'], $join['relation'], $joinTableType);
			}
		}
		$this->db->where($where);
		if ($groupBy != '') {
			$this->db->group_by($groupBy);
		}
		$this->db->order_by($order, $orderBy);
		if (!is_null($limit)) $this->db->limit($limit[0], $limit[1]);
		$query = $this->db->get();
		return $query->num_rows();
		//return $this->db->last_query();
	}

	function readDataRow($select, $from, $where, $joinTable, $joinTableType = 'LEFT', $groupBy, $order, $orderBy, $limit = null)
	{
		$this->db->select('SQL_CALC_FOUND_ROWS ' . $select, false);
		$this->db->from($from);
		if (count((array) $joinTable)) {
			foreach ($joinTable as $join) {
				$this->db->join($join['table'], $join['relation'], $joinTableType);
			}
		}
		$this->db->where($where);
		if ($groupBy != '') {
			$this->db->group_by($groupBy);
		}
		$this->db->order_by($order, $orderBy);
		if (!is_null($limit)) $this->db->limit($limit[0], $limit[1]);
		$query = $this->db->get();
		return $query->row();
		//return $this->db->last_query();
	}

	function updateData($table, $data, $where)
	{
		$this->db->where($where);
		return $this->db->update($table, $data);
	}

	function updateDataBatch($table, $data, $where)
	{
		return $this->db->update_batch($table, $data, $where);
	}

	function deleteData($table, $where)
	{
		$this->db->where($where);
		$this->db->delete($table);
		// return $this->db->error();
	}

	function multipleDelete($table, $field, $code)
	{
		$this->db->where($field, $code);
		$this->db->delete($table);
		// return $this->db->error();
	}

	# Start Data table
	function dataTableQuery($select, $table, $where, $column_search, $column_order, $order, $joinTable, $joinTableType = 'LEFT')
	{
		$this->db->select($select);
		$this->db->from($table);
		$this->db->where($where);
		if (count((array) $joinTable)) {
			foreach ($joinTable as $join) {
				$this->db->join($join['table'], $join['relation'], $joinTableType);
			}
		}
		$i = 0;
		foreach ($column_search as $item) {
			if ($_POST['search']['value']) {
				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}
				if (count($column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}

		if (isset($_POST['order'])) {
			$this->db->order_by($column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($order)) {
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	function getDatatable($select, $table, $where, $column_search, $column_order, $order, $joinTable, $joinTableType = 'LEFT', $groupBy = '')
	{
		$this->dataTableQuery($select, $table, $where, $column_search, $column_order, $order, $joinTable, $joinTableType);
		if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		if ($groupBy != '') {
			$this->db->group_by($groupBy);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function dataTableCount($table, $where, $joinTable, $groupBy = '', $select = '')
	{
		if ($select != '') {
			$this->db->select($select);
		}
		$this->db->from($table);
		if (count((array) $joinTable)) {
			foreach ($joinTable as $join) {
				$this->db->join($join['table'], $join['relation'], 'LEFT');
			}
		}
		$this->db->where($where);
		if ($groupBy != '') {
			$this->db->group_by($groupBy);
		}
		return $this->db->count_all_results();
	}

	function dataTableFilter($select, $table, $where, $column_search, $column_order, $order, $joinTable, $joinTableType = 'LEFT', $groupBy = '')
	{
		$this->dataTableQuery($select, $table, $where, $column_search, $column_order, $order, $joinTable, $joinTableType);
		if ($groupBy != '') {
			$this->db->group_by($groupBy);
		}
		$query = $this->db->get();
		return $query->num_rows();
	}
	# End Data table


	# Start Data Table Metronic
	function getDataTableMetronic($select, $table, $where, $column_search)
	{
		$this->dataTableMetronicQuery($select, $table, $where, $column_search);
		if ($_POST['pagination']['perpage'] != -1) {
			$this->db->limit($_POST['pagination']['perpage'], $_POST['pagination']['perpage'] * $_POST['pagination']['page'] - $_POST['pagination']['perpage']);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function dataTableMetronicQuery($select, $table, $where, $column_search)
	{
		$this->db->select($select);
		$this->db->from($table);
		$this->db->where($where);
		$this->db->order_by($_POST['sort']['field'], $_POST['sort']['sort']);

		$i = 0;
		foreach ($column_search as $item) {
			if (isset($_POST['query']['generalSearch'])) {
				if ($i === 0) {
					$this->db->group_start();
					$this->db->like($item, $_POST['query']['generalSearch']);
				} else {
					$this->db->or_like($item, $_POST['query']['generalSearch']);
				}
				if (count($column_search) - 1 == $i) //last loop
					$this->db->group_end(); //close bracket
			}
			$i++;
		}
	}

	function dataTableMetronicCount($select, $table, $where, $column_search)
	{
		$this->dataTableMetronicQuery($select, $table, $where, $column_search);
		$query = $this->db->get();
		return $query->num_rows();
	}

	function dataTableMetronicFilter($select, $table, $where, $column_search)
	{
		$totalRecord = $this->dataTableMetronicCount($select, $table, $where, $column_search);
		$page = $totalRecord / $_POST['pagination']['perpage'];
		$pages = ceil($page / 1) * 1;
		return $pages;
	}
	# End Data Table Metronic
}

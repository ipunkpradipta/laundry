	<!-- Vendor -->
	<!-- <script src="<?= base_url('assets/klorofilpro')?>/assets/js/vendor.min.js"></script> -->

	<!-- Plugins -->
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/jquery-jeditable/jquery.jeditable.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/jquery.maskedinput/jquery.maskedinput.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/jquery-jeditable/jquery.jeditable.masked.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/jquery-ui/jquery-ui.min.js"></script> <!-- required by datepicker plugin -->
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/jquery-jeditable/jquery.jeditable.datepicker.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/jqvmap/jquery.vmap.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/jqvmap/maps/jquery.vmap.world.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/flot/jquery.canvaswrapper.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/flot/jquery.colorhelpers.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/flot/jquery.flot.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/flot/jquery.flot.saturated.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/flot/jquery.flot.browser.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/flot/jquery.flot.drawSeries.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/flot/jquery.flot.uiConstants.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/flot/jquery.flot.resize.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/flot/jquery.flot.legend.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/flot/jquery.flot.hover.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/flot/jquery.flot.time.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/jquery.flot.tooltip/jquery.flot.tooltip.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/justgage/raphael.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/justgage/justgage.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/datatables.net/jquery.dataTables.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/datatables.net-bs4/dataTables.bootstrap4.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/jquery.appear/jquery.appear.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/bootstrap-tour/bootstrap-tour-standalone.js"></script>

	<!-- Datables Core -->
	<!-- <script src="assets/plugins/datatables.net/jquery.dataTables.min.js"></script>
	<script src="assets/plugins/datatables.net-bs4/dataTables.bootstrap4.min.js"></script> -->

	<!-- Datables Extension -->
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/datatables.net-buttons/js/buttons.html5.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/datatables.net-buttons/js/buttons.print.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/jszip/jszip.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/pdfmake/pdfmake.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/pdfmake/vfs_fonts.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/datatables.net-buttons-bs4/buttons.bootstrap4.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/datatables.net-colreorder/dataTables.colReorder.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/datatables.net-colreorder-bs4/colReorder.bootstrap4.min.js"></script>

	<!-- Init -->
	<script src="<?= base_url('assets/klorofilpro')?>/assets/js/pages/dashboard.init.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/js/pages/ui-dragdroppanel.init.min.js"></script>
	<script src="<?= base_url('assets/klorofilpro')?>/assets/js/pages/tables-dynamic.init.min.js"></script>

	<!-- App -->
	<script src="<?= base_url('assets/klorofilpro')?>/assets/js/app.min.js"></script>
</body>
</html>
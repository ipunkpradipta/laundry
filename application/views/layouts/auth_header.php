
<!doctype html>
<html lang="en" class="fullscreen-bg">
  <head>
    <title>Laundry App | <?=$title?></title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <!-- App css -->
    <link href="<?= base_url('assets')?>/klorofilpro/assets/css/bootstrap-custom.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('assets')?>/klorofilpro/assets/css/app.min.css" rel="stylesheet" type="text/css" />
    
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" rel="stylesheet">
    <link href="<?= base_url('assets')?>/klorofilpro/assets/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css" />
    <script src="<?= base_url('assets/klorofilpro')?>/assets/js/vendor.min.js"></script>
    <script src="<?= base_url('assets/klorofilpro')?>/assets/plugins/toastr/toastr.min.js"></script>
    <!-- Favicon -->
    <link rel="shortcut icon" href="<?= base_url('assets')?>/klorofilpro/assets/images/favicon.png">
    <script>
      function notification(type,message){
        toastr.remove();
          toastr.options.toastClass = "toastr-plugin"; // must be set to avoid class conflict with Bootstrap
          toastr.options.timeOut = "false";
          toastr.options.closeButton = true;
          toastr[type](message);
          setTimeout(() => {
            toastr.remove();
          }, 3000);
      }
    </script>
  </head>
  <body>
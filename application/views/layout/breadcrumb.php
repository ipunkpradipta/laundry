<!-- start page title -->
<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <?php
                        $list = '';
                        foreach($breadcrumb['right'] as $li){
                            $list .= '<li class="breadcrumb-item">'.$li.'</li>';
                        }
                        echo $list;
                    ?>
                </ol>
            </div>
            <h4 class="page-title"><?=$breadcrumb['left']?></h4>
        </div>
    </div>
</div>     
<!-- end page title --> 
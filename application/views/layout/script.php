<!-- Vendor js -->
<script src="<?= base_url('assets/ubold') ?>/assets/js/vendor.min.js"></script>

<!-- Plugins js-->
<script src="<?= base_url('assets/ubold') ?>/assets/libs/flatpickr/flatpickr.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/apexcharts/apexcharts.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/selectize/js/standalone/selectize.min.js"></script>

<!-- third party js -->
<script src="<?= base_url('assets/ubold') ?>/assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/datatables.net-buttons/js/buttons.flash.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/datatables.net-select/js/dataTables.select.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/pdfmake/build/pdfmake.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/pdfmake/build/vfs_fonts.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/jquery-toast-plugin/jquery.toast.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/devbridge-autocomplete/jquery.autocomplete.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/flatpickr/flatpickr.min.js"></script>
<!-- <script src="<?= base_url('assets/ubold') ?>/assets/js/pages/toastr.init.js"></script> -->
<!-- third party js ends -->

<!-- Datatables init -->
<!-- <script src="<?= base_url('assets/ubold') ?>/assets/js/pages/datatables.init.js"></script> -->

<!-- Dashboar 1 init js-->
<!-- <script src="<?= base_url('assets/ubold') ?>/assets/js/pages/dashboard-1.init.js"></script> -->

<!-- App js-->
<script src="<?= base_url('assets/ubold') ?>/assets/js/app.min.js"></script>
<!-- Menu & User Section Script -->
<script>
	function notif_success(title, message, icon) {
		$.toast({
			heading: title,
			text: message,
			position: 'top-right',
			loaderBg: '#5ba035',
			icon: 'success',
			hideAfter: 3e3,
			stack: 1
		});
	}

	function notif_danger(title, message) {
		$.toast({
			heading: title,
			text: message,
			position: 'top-center',
			loaderBg: '#5ba035',
			icon: 'danger',
			hideAfter: 3e3,
			stack: 1
		});
	}

	const tableMenu = $("#table_menu").DataTable({
		language: {
			paginate: {
				previous: "<i class='mdi mdi-chevron-left'>",
				next: "<i class='mdi mdi-chevron-right'>",
			},
		},
		drawCallback: function() {
			$(".dataTables_paginate > .pagination").addClass("pagination-rounded");
		},
		serverSide: true,
		proccessing: true,
		destroy: true,
		ajax: {
			url: '<?= base_url() ?>master/menu/getAllMenu',
			method: 'POST'
		}
	});

	$('#form_menu_add').on('submit', (e) => {
		e.preventDefault();
		const formData = $('#form_menu_add').serialize();
		$.ajax({
			url: '<?= base_url() ?>master/menu/postMenu',
			method: 'POST',
			data: formData,
			dataType: 'json',
			beforeSend: () => {
				$('#menu-submit-btn').hide();
				$('#btn-spinner').show();
			},
			success: (result) => {
				console.log(result)
				$('#menu-submit-btn').show();
				$('#btn-spinner').hide();
				$('#modalAddMenu').modal('hide');
				if (result.status) {
					tableMenu.ajax.reload();
					notif_success('Sukses!', result.message);
					$('#form_menu_add').get(0).reset();
				} else {
					notif_danger('Gagal!', result.message)
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				$('#menu-submit-btn').show();
				$('#btn-spinner').hide();
				alert(xhr.status);
				alert(thrownError);
			}
		})
	})

	function setIdMenu(id_menu) {
		$('#id_menu').val(id_menu);
		const tableSubMenu = $("#table_submenu").DataTable({
			language: {
				paginate: {
					previous: "<i class='mdi mdi-chevron-left'>",
					next: "<i class='mdi mdi-chevron-right'>",
				},
			},
			drawCallback: function() {
				$(".dataTables_paginate > .pagination").addClass("pagination-rounded");
			},
			serverSide: true,
			proccessing: true,
			destroy: true,
			ajax: {
				url: '<?= base_url() ?>master/menu/getSubMenu',
				method: 'POST',
				data: {
					id_menu
				},
				dataType: 'json'
			}
		});
	}

	function setForm(value) {
		if (value == 'menu') {
			$('#id_menu').val('');
		}
		$('#modalListSubmenu').modal('hide');
		$('#form_input').val(value);
	}

	const tableUser = $("#table_user").DataTable({
		language: {
			paginate: {
				previous: "<i class='mdi mdi-chevron-left'>",
				next: "<i class='mdi mdi-chevron-right'>",
			},
		},
		drawCallback: function() {
			$(".dataTables_paginate > .pagination").addClass("pagination-rounded");
		},
		serverSide: true,
		proccessing: true,
		destroy: true,
		ajax: {
			url: '<?= base_url() ?>master/user/getAllUser',
			method: 'POST'
		}
	});

	function openRoleModal(idUser) {
		$('#modalRoleAccess').modal('show');
		$.ajax({
			url: '<?= base_url() ?>master/user/getRoleMenu',
			type: 'post',
			data: {
				idUser
			},
			dataType: 'json',
			success: (result) => {
				if (result.status) {
					let html = `<div class="row" style="border-bottom:1px solid black;">
					<div class="col-md-6">
					Menu
					</div>
					<div class="col-md-6">
					Submenu
					</div>
					</div>`;
					result.data.map((value, index) => {
						let submenu = '';
						html += `
						<div class="row mt-2" style="border-bottom:1px solid black;" id=listRoleMenu>
							<div class="col-md-6">
								<div class="form-check mb-3">
									<input type="checkbox" class="form-check-input" id="is_parent" name="is_parent" onchange="checkPrevilageMenu('menu',${idUser},${value.menuId})" ${ (value.menuAsign) ? "checked" : ""}>
									<label class="form-check-label" for="is_parent">${value.menuName}</label>
								</div>
							</div>
							<div class="col-md-6">
								${
									value.submenu.map((valueSubmenu,indexSubmenu) =>{
										submenu += `
											<div class="form-check mb-3">
												<input type="checkbox" class="form-check-input" id="is_parent" name="is_parent" onchange="checkPrevilageMenu('submenu',${idUser},${value.menuId},${valueSubmenu.id})" ${ (valueSubmenu.assigned) ? "checked" : ""}>
												<label class="form-check-label" for="is_parent">${valueSubmenu.name}</label>
											</div>
										`
									})
								}
								${submenu}
							</div>
						</div>`
					})
					$('.modal-body-role').html(html)
				}
			}
		})
	}

	function checkPrevilageMenu(mapping, userId, menuId, submenuId = null) {
		let payload = {}
		if (!submenuId) {
			payload = {
				mapping,
				userId,
				menuId
			}
		} else {
			payload = {
				mapping,
				userId,
				menuId,
				submenuId
			}
		}

		$.ajax({
			url: '<?= base_url() ?>master/user/updateRoleMenu',
			type: 'post',
			data: payload,
			dataType: 'json',
			success: (result) => {
				console.log(result);
				if (result.status) {
					notif_success('Sukses!', result.message);
				}
			}
		})
	}
</script>
<!-- End Menu & User Section Script -->
<!-- Customer Section Script -->
<script>
	const tableCustomer = $("#table_customer").DataTable({
		language: {
			paginate: {
				previous: "<i class='mdi mdi-chevron-left'>",
				next: "<i class='mdi mdi-chevron-right'>",
			},
		},
		drawCallback: function() {
			$(".dataTables_paginate > .pagination").addClass("pagination-rounded");
		},
		serverSide: true,
		proccessing: true,
		destroy: true,
		ajax: {
			url: '<?= base_url() ?>master/customer/getAllCustomer',
			method: 'POST'
		}
	});

	$('#form_customer_add').on('submit', (e) => {
		e.preventDefault();
		const formData = $('#form_customer_add').serialize();
		$.ajax({
			url: '<?= base_url() ?>master/customer/postCustomer',
			method: 'POST',
			data: formData,
			dataType: 'json',
			beforeSend: () => {
				$('#menu-submit-btn').hide();
				$('#btn-spinner').show();
			},
			success: (result) => {
				console.log(result)
				$('#menu-submit-btn').show();
				$('#btn-spinner').hide();
				$("#modalAddMenu .btn-close").click()
				if (result.status) {
					tableCustomer.ajax.reload();
					notif_success('Sukses!', result.message);
					$('#form_customer_add').get(0).reset();
				} else {
					tableCustomer.ajax.reload();
					$('#form_customer_add').get(0).reset();
					notif_danger('Gagal!', result.message)
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				$('#menu-submit-btn').show();
				$('#btn-spinner').hide();
				$("#modalAddMenu .btn-close").click()
				alert(xhr.status);
				alert(thrownError);
			}
		})
	})
</script>
<!-- End Customer Section Script -->
<!-- Product Section Script -->
<script>
	const tableProduct = $("#table_product").DataTable({
		language: {
			paginate: {
				previous: "<i class='mdi mdi-chevron-left'>",
				next: "<i class='mdi mdi-chevron-right'>",
			},
		},
		drawCallback: function() {
			$(".dataTables_paginate > .pagination").addClass("pagination-rounded");
		},
		serverSide: true,
		proccessing: true,
		destroy: true,
		ajax: {
			url: '<?= base_url() ?>master/product/getAllProduct',
			method: 'POST'
		}
	});

	$('#form_product_add').on('submit', (e) => {
		e.preventDefault();
		const formData = $('#form_product_add').serialize();
		$.ajax({
			url: '<?= base_url() ?>master/product/postProduct',
			method: 'POST',
			data: formData,
			dataType: 'json',
			beforeSend: () => {
				$('#menu-submit-btn').hide();
				$('#btn-spinner').show();
			},
			success: (result) => {
				console.log(result)
				$('#menu-submit-btn').show();
				$('#btn-spinner').hide();
				$("#modalAddProduct .btn-close").click()
				if (result.status) {
					tableProduct.ajax.reload();
					notif_success('Sukses!', result.message);
					$('#form_product_add').get(0).reset();
				} else {
					tableProduct.ajax.reload();
					$('#form_product_add').get(0).reset();
					notif_danger('Gagal!', result.message)
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				$('#menu-submit-btn').show();
				$('#btn-spinner').hide();
				$("#modalAddProduct .btn-close").click()
				alert(xhr.status);
				alert(thrownError);
			}
		})
	})
</script>
<!-- End Product Section Script -->
<!-- Cart Section Script -->
<script>
	$("#no_handphone").autocomplete({
		serviceUrl: '<?= base_url() ?>/core/nota/getAllCustomer',
		onSelect: function(e) {
			const splitValue = e.value.split('-')
			$("#no_handphone").val(splitValue[0])
			$("#nama_konsumen").val(splitValue[1])
			$("#id_konsumen").val(e.data)
		},
	});
	$("#nama_produk").autocomplete({
		serviceUrl: '<?= base_url() ?>/core/nota/getAllProducts',
		onSelect: function(e) {
			$('#id_produk').val(e.data.id)
			$('#harga_produk').val(e.data.harga)
		},
	});

	$('#form_nota_add_cart').on('submit', (e) => {
		e.preventDefault();
		const formData = $('#form_nota_add_cart').serialize();
		$.ajax({
			url: '<?= base_url() ?>/core/nota/postCart',
			type: 'POST',
			data: formData,
			dataType: 'json',
			beforeSend: () => {
				$('#menu-submit-btn').hide();
				$('#btn-spinner').show();
			},
			success: (result) => {
				$('#menu-submit-btn').show();
				$('#btn-spinner').hide();
				if (result.status) {
					tableCart.ajax.reload();
					notif_success('Sukses!', result.message);
					$('#form_nota_add_cart').get(0).reset();
				} else {
					notif_danger('Gagal!', result.message)
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				$('#menu-submit-btn').show();
				$('#btn-spinner').hide();
				alert(xhr.status);
				alert(thrownError);
			}
		})
	})

	let idKeranjang = [];
	const tableCart = $("#table_cart").DataTable({
		language: {
			paginate: {
				previous: "<i class='mdi mdi-chevron-left'>",
				next: "<i class='mdi mdi-chevron-right'>",
			},
		},
		drawCallback: function(settings) {
			$(".dataTables_paginate > .pagination").addClass("pagination-rounded");
			var response = settings.json;
			idKeranjang = [];
			response.data.map((value, index) => {
				idKeranjang.push(value[7])
			})
			$('#table_cart tfoot').html('');
			if (response.voucher_code !== '') {
				$("#table_cart").append(
					$('<tfoot/>').append(
						`<tr>
							<th colspan=5 style="text-align:right">Tagihan</th>
							<th id="sumTotal">${response.sumTotal}</th>
						</tr>
						<tr>
							<th colspan=5 style="text-align:right">Voucher Discount</th>
							<th id="voucher_discount">${response.voucher_discount}</th>
							<th>
								<div class="ribbon ribbon-secondary float-end">
									<i class="mdi mdi-sale me-1"></i> 
									${response.voucher_code} 
									<i class="mdi mdi-close" style="margin:-12px 0px 0px 10px !important;position:absolute; cursor:pointer" onclick="removeVoucherCode()"></i>
								</div>
							</th>
						</tr>
						<tr>
							<th colspan=5 style="text-align:right">Ditagihkan</th>
							<th id="grandTotal">${response.grandTotal}</th>
						</tr>
						<tr>
							<th colspan=6 style="text-align:right">
								<button type="button" class="btn btn-soft-primary waves-effect waves-light" onclick="return handlePrintButton()">Cetak</button>
							</th>
						</tr>`
					)
				);
			} else {
				$("#table_cart").append(
					$('<tfoot/>').append(
						`<tr>
							<th colspan=5 style="text-align:right">Tagihan</th>
							<th id="sumTotal">${response.sumTotal}</th>
						</tr>
						<tr>
							<th colspan=5 style="text-align:right">Voucher Discount</th>
							<th id="voucher_discount">
									<input type="text" id="kode_voucher" name="kode_voucher" class="form-control form-control-sm" placeholder="Kode Voucher" onkeyup="handleVoucherCode(event,this,'${response.sumTotal}')">
							</th>
						</tr>
						<tr>
							<th colspan=5 style="text-align:right">Ditagihkan</th>
							<th id="grandTotal">${response.grandTotal}</th>
						</tr>
						<tr>
							<th colspan=6 style="text-align:right">
								<button type="button" class="btn btn-soft-primary waves-effect waves-light" onclick="return handlePrintButton()">Cetak</button>
							</th>
						</tr>`
					)
				);
			}
		},
		serverSide: true,
		proccessing: true,
		searching: false,
		info: false,
		paging: false,
		destroy: true,
		ajax: {
			url: '<?= base_url() ?>core/nota/getAllCart?voucherCode=',
			method: 'POST'
		}
	});

	const handleVoucherCode = (event, element, tagihan) => {
		const voucher_code = $(element).val();
		const regex = /[\d|,|e|E|\+]+/g;
		const raw_total_tagihan = tagihan.split(',')[0].match(regex);
		let totalTagihan = '';
		raw_total_tagihan.map((value, index) => {
			totalTagihan += value
		})
		if (event.keyCode == 13) {
			$.ajax({
				url: '<?= base_url() ?>core/nota/validationVoucherCode',
				type: 'POST',
				data: {
					voucher_code,
					totalTagihan
				},
				dataType: 'JSON',
				success: (result) => {
					console.log(result);
					if (result.status) {
						notif_success('Sukses!', result.message);
					} else {
						notif_danger('Gagal!', result.message)
					}
					tableCart.ajax.reload();
					tableCart.ajax.url(`<?= base_url() ?>core/nota/getAllCart?value=${result.value}&voucherCode=${result.voucher_code}`).load();
				}
			})
		}
	}

	const removeVoucherCode = () => {
		tableCart.ajax.url(`<?= base_url() ?>core/nota/getAllCart?voucherCode=`).load();
	}

	const deleteItemCart = (id) => {
		$.ajax({
			url: '<?= base_url() ?>core/nota/deleteCart',
			type: 'POST',
			data: {
				id: id
			},
			dataType: 'json',
			success: (result) => {
				if (result.status) {
					tableCart.ajax.reload();
					notif_success('Sukses!', result.message);
				} else {
					notif_danger('Gagal!', result.message)
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(xhr.status);
				alert(thrownError);
			}
		})
	}

	const handlePrintButton = () => {
		// const formCart = $('#nota').find('input').serialize();

		const no_handphone = $('#no_handphone').val();
		const nama_konsumen = $('#nama_konsumen').val();
		const id_konsumen = $('#id_konsumen').val();
		const tagihan = $('#sumTotal').text();
		let voucher = $('#voucher_discount').text().trim();
		const ditagihkan = $('#grandTotal').text();
		// console.log('tagihan', tagihan);
		// console.log('voucher', voucher);
		// console.log('ditagihkan', ditagihkan);
		if (!voucher) {
			voucher = 0;
		}
		console.log('keranjang_id', idKeranjang);
		if (!no_handphone) {
			return notif_danger('Gagal!', 'Kolom Konsumen harus diisi!')
		}
		$.ajax({
			url: '<?= base_url() ?>core/nota/createNota',
			type: 'POST',
			data: {
				id_konsumen,
				nama_konsumen,
				no_handphone,
				tagihan,
				voucher,
				ditagihkan,
				idKeranjang
			},
			dataType: 'json',
			success: (result) => {
				console.log(result);
				if (result.status) {
					tableCart.ajax.reload();
					$('#form_nota_customer')[0].reset();
					notif_success('Sukses!', result.message);
				} else {
					notif_danger('Gagal!', result.message)
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(xhr.status);
				alert(thrownError);
			}
		})
	}
</script>
<!-- End Cart Section Script -->
<!-- Voucher Section Script -->
<script>
	$('#valid_date_start').flatpickr({
		minDate: "today"
	});
	$('#valid_date_end').flatpickr({
		minDate: "today"
	});
	const tableVoucher = $("#table_voucher").DataTable({
		language: {
			paginate: {
				previous: "<i class='mdi mdi-chevron-left'>",
				next: "<i class='mdi mdi-chevron-right'>",
			},
		},
		drawCallback: function() {
			$(".dataTables_paginate > .pagination").addClass("pagination-rounded");
		},
		serverSide: true,
		proccessing: true,
		destroy: true,
		ajax: {
			url: '<?= base_url() ?>core/voucher/getAllVoucher',
			method: 'POST'
		}
	});

	$('#form_voucher_add').on('submit', (e) => {
		e.preventDefault();
		const formData = $('#form_voucher_add').serialize();
		$.ajax({
			url: '<?= base_url() ?>/core/voucher/postVoucher',
			type: 'POST',
			data: formData,
			dataType: 'json',
			beforeSend: () => {
				$('#menu-submit-btn').hide();
				$('#btn-spinner').show();
			},
			success: (result) => {
				$('#menu-submit-btn').show();
				$('#btn-spinner').hide();
				$("#modalCreateVoucher .btn-close").click()
				if (result.status) {
					tableVoucher.ajax.reload();
					notif_success('Sukses!', result.message);
					$('#form_voucher_add').get(0).reset();
				} else {
					notif_danger('Gagal!', result.message)
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				$("#modalCreateVoucher .btn-close").click()
				$('#menu-submit-btn').show();
				$('#btn-spinner').hide();
				alert(xhr.status);
				alert(thrownError);
			}
		})
	})
</script>
<!-- End Voucher Section Script -->

<!-- Nota Section Script -->
<script>
	const tableNota = $("#table_nota").DataTable({
		language: {
			paginate: {
				previous: "<i class='mdi mdi-chevron-left'>",
				next: "<i class='mdi mdi-chevron-right'>",
			},
		},
		drawCallback: function() {
			$(".dataTables_paginate > .pagination").addClass("pagination-rounded");
		},
		serverSide: true,
		proccessing: true,
		destroy: true,
		ajax: {
			url: '<?= base_url() ?>core/nota/getAllNota',
			method: 'POST'
		}
	});

	const detailNotaByid = id => {
		$.ajax({
			url: '<?= base_url() ?>core/nota/getNotaById',
			type: 'POST',
			data: {
				id
			},
			dataType: 'json',
			success: (result) => {
				if (result.status) {
					$('#full-width-modal').modal('show');
					$('#fullWidthModalLabel').text('Detail Nota ' + result.data.no_nota)
					console.log(result)
					const nota = result.data
					$('.status_nota').text(nota.status_proses_terakhir)
					$('.tgl_nota').text(nota.tgl_nota)
					$('.tgl_trx').text(nota.created_date)
					$('.customer_name').text(nota.customer_name)
					$('.jumlah_tagihan').text(nota.jumlah_tagihan)
					$('.jumlah_potongan').text(nota.jumlah_potongan)
					$('.jumlah_ditagihkan').text(nota.jumlah_ditagihkan)
					let string_item = '';
					nota.item.map((value, index) => {
						string_item += `
						<tr>
							<td>${value.product_name}</td>
							<td>${value.total_item}</td>
							<td>${value.banyaknya}</td>
							<td>${value.harga_satuan}</td>
							<td>${value.harga_total}</td>
						</tr>
						`;
					})
					$('.body_item').html(string_item);
				}
				// $('#modal_body').html(`
				// <div class="row">
				// 	<div class="col-md-6" style="display: flex; justify-content: start; align-items: center;">Customer Name</div>
				// 	<div class="col-md-6" style="display: flex; justify-content: end; align-items: center;">ABC.com</div>
				// </div>`);
			}
		})
	}
</script>
<!-- End Nota Section Script -->
<!-- Plugins css -->
<link href="<?= base_url('assets/ubold')?>/assets/libs/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url('assets/ubold')?>/assets/libs/selectize/css/selectize.bootstrap3.css" rel="stylesheet" type="text/css" />
<!-- third party css -->
<link href="<?= base_url('assets/ubold')?>/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url('assets/ubold')?>/assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url('assets/ubold')?>/assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url('assets/ubold')?>/assets/libs/datatables.net-select-bs4/css//select.bootstrap4.min.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url('assets/ubold')?>/assets/libs/jquery-toast-plugin/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<link href="<?= base_url('assets/ubold')?>/assets/libs/flatpickr/flatpickr.min.css" rel="stylesheet" type="text/css" />
<!-- third party css end -->

<!-- App css -->
<link href="<?= base_url('assets/ubold')?>/assets/css/config/default/bootstrap.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
<link href="<?= base_url('assets/ubold')?>/assets/css/config/default/app.min.css" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

<link href="<?= base_url('assets/ubold')?>/assets/css/config/default/bootstrap-dark.min.css" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" />
<link href="<?= base_url('assets/ubold')?>/assets/css/config/default/app-dark.min.css" rel="stylesheet" type="text/css" id="app-dark-stylesheet" />

<!-- icons -->
<link href="<?= base_url('assets/ubold')?>/assets/css/icons.min.css" rel="stylesheet" type="text/css" />


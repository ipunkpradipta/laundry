<!-- ========== Left Sidebar Start ========== -->
<div class="left-side-menu">

    <div class="h-100" data-simplebar>

        <!-- User box -->
        <div class="user-box text-center">
            <img src="<?= base_url() ?>/assets/images/<?= $this->session->userdata('user_detail')['user_image'] ?>" alt="user-img" title="Mat Helme" class="rounded-circle avatar-md">
            <div class="dropdown">
                <a href="javascript: void(0);" class="text-dark dropdown-toggle h5 mt-2 mb-1 d-block" data-bs-toggle="dropdown"><?= $this->session->userdata('user_detail')['user_nama'] ?></a>
                <div class="dropdown-menu user-pro-dropdown">

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-user me-1"></i>
                        <span>My Account</span>
                    </a>

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-settings me-1"></i>
                        <span>Settings</span>
                    </a>

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-lock me-1"></i>
                        <span>Lock Screen</span>
                    </a>

                    <!-- item-->
                    <a href="javascript:void(0);" class="dropdown-item notify-item">
                        <i class="fe-log-out me-1"></i>
                        <span>Logout</span>
                    </a>

                </div>
            </div>
            <p class="text-muted"><?= ($this->session->userdata('user_detail')['user_is_admin'] == "1") ? "Admin" : "Staff" ?></p>
        </div>

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul id="side-menu">
                <li class="menu-title">Navigation</li>
                <?php
                // print_r($this->session->userdata('user_menu'));
                $rawMenu = (object) $this->session->userdata('user_menu');
                $listMenu = json_decode(json_encode($rawMenu), FALSE);
                // $listMenu = (object) $this->session->userdata('user_menu');
                foreach ($listMenu as $menu) :
                    if ($menu->isParent == 0) :
                ?>

                        <li>
                            <a href="<?= base_url() . $menu->menuUrl ?>">
                                <?= $menu->menuIcon ?>
                                <span> <?= $menu->menuName ?> </span>
                            </a>
                        </li>
                    <?php else : ?>
                        <li>
                            <a href="#sidebar<?= $menu->menuName ?>" data-bs-toggle="collapse">
                                <?= $menu->menuIcon ?>
                                <!-- <span class="badge bg-success rounded-pill float-end">4</span> -->
                                <span> <?= $menu->menuName ?> </span>
                                <span class="menu-arrow"></span>
                            </a>
                            <div class="collapse" id="sidebar<?= $menu->menuName ?>">
                                <ul class="nav-second-level">
                                    <?php
                                    foreach ($menu->submenu as $submenu) :
                                    ?>
                                        <li>
                                            <a href="<?= base_url() . $submenu->submenuUrl ?>"><?= $submenu->submenuName ?></a>
                                        </li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </li>
                    <?php endif ?>
                <?php endforeach ?>
            </ul>
        </div>
        <!-- End Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>
<!-- Left Sidebar End -->
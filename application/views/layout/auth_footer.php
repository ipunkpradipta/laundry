<footer class="footer footer-alt">
  2015 - <script>
    document.write(new Date().getFullYear())
  </script> &copy; UBold theme by <a href="" class="text-white-50">Coderthemes</a>
</footer>

<!-- Vendor js -->
<script src="<?= base_url('assets/ubold') ?>/assets/js/vendor.min.js"></script>

<!-- App js -->
<script src="<?= base_url('assets/ubold') ?>/assets/js/app.min.js"></script>
<script src="<?= base_url('assets/ubold') ?>/assets/libs/jquery-toast-plugin/jquery.toast.min.js"></script>

<script>
  function notif_success(title, message) {
    $.toast({
      heading: title,
      text: message,
      position: 'top-right',
      loaderBg: '#5ba035',
      icon: 'success',
      hideAfter: 3e3,
      stack: 1
    });
  }

  function notif_danger(title, message) {
    $.toast({
      heading: title,
      text: message,
      position: 'top-right',
      loaderBg: '#5ba035',
      icon: 'danger',
      hideAfter: 3e3,
      stack: 1
    });
  }
</script>
<script>
  const base_url = '<?= base_url(); ?>';
  $('#form_login').on('submit', (e) => {
    e.preventDefault();
    const formData = $('#form_login').serialize();
    $.ajax({
      url: base_url + 'auth/login',
      type: 'POST',
      dataType: 'json',
      data: formData,
      success: function(result) {
        console.log(result.status);
        if (result.status) {
          notif_success('success', result.message);
          setTimeout(() => {
            window.location = base_url;
          }, 1000);
          return
        }
        notif_danger('error', result.message);
      },
      error: function(xhr, ajaxOptions, thrownError) {
        alert(xhr.status);
        alert(thrownError);
        notif_danger('error', thrownError);
      }
    });
  })
</script>

</body>

</html>
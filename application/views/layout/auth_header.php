<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <title>Log In | UBold - Responsive Admin Dashboard Template</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
  <meta content="Coderthemes" name="author" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- App favicon -->
  <link rel="shortcut icon" href="<?= base_url('assets/ubold') ?>/assets/images/favicon.ico">

  <!-- App css -->
  <link href="<?= base_url('assets/ubold') ?>/assets/css/config/default/bootstrap.min.css" rel="stylesheet" type="text/css" id="bs-default-stylesheet" />
  <link href="<?= base_url('assets/ubold') ?>/assets/css/config/default/app.min.css" rel="stylesheet" type="text/css" id="app-default-stylesheet" />

  <link href="<?= base_url('assets/ubold') ?>/assets/css/config/default/bootstrap-dark.min.css" rel="stylesheet" type="text/css" id="bs-dark-stylesheet" />
  <link href="<?= base_url('assets/ubold') ?>/assets/css/config/default/app-dark.min.css" rel="stylesheet" type="text/css" id="app-dark-stylesheet" />
  <link href="<?= base_url('assets/ubold') ?>/assets/libs/jquery-toast-plugin/jquery.toast.min.css" rel="stylesheet" type="text/css" />

  <!-- icons -->
  <link href="<?= base_url('assets/ubold') ?>/assets/css/icons.min.css" rel="stylesheet" type="text/css" />

  <script src="<?= base_url('assets/ubold') ?>/assets/libs/jquery-toast-plugin/jquery.toast.min.js"></script>
  <script>
    function notif_success(title, message) {
      $.toast({
        heading: title,
        text: message,
        position: 'top-right',
        loaderBg: '#5ba035',
        icon: 'success',
        hideAfter: 3e3,
        stack: 1
      });
    }

    function notif_danger(title, message) {
      $.toast({
        heading: title,
        text: message,
        position: 'top-right',
        loaderBg: '#5ba035',
        icon: 'danger',
        hideAfter: 3e3,
        stack: 1
      });
    }
  </script>

</head>

<body class="loading authentication-bg authentication-bg-pattern">
# Laundry APP

1. Asana Project -> https://app.asana.com/0/1188784975794564/board
2. Application server ->
3. Database server ->
4. Installasi:

```bash
composer install
```

```bash
create .htaccess file
```

htaccess file

```bash
<IfModule mod_rewrite.c>
  RewriteEngine On
  #RewriteBase /

  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteRule ^ index.php [QSA,L]
</IfModule>
```

Edit Config apache2, set AllowOverride All for directory "www"

```bash
sudo nano /etc/apache2/apache2.conf
```

Enabled apache mod rewrite (Command)

```bash
sudo a2enmod rewrite
```

Restart Apache (Command)

```bash
sudo /etc/init.d/apache2 restart
```

```bash
copy structure table in dbconvertermanifest.sql
```

```bash
edit file php.ini and set max_execution_time=300
```

restart apche2 dan php
